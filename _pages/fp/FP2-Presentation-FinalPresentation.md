---
onepage-id: "fp"
title: "Final Presentation"
nav-color: "sem2"
order: 52
---

## Title

  I have designed a representation of post human life: a family of creatures that brings the future of non-organic creatures into today.
  These creatures are designed specially to influence you emotions through interaction, and provoke questions about life, technology and their intersections.
  <!-- These creatures are the initial creations of one of many species that will come to light in the future. This species inhabits the deep ocean, and are unlike organic animals we are used to, but *different from their robotic human serving counterparts*. -->

  So let's let these creatures rest for the time being whilst I walk you through my journey.
## how did i get here
I began this project wanting to explore questions about life, and the future of robotics.
I was fascinated by the interactions between robot and humans, particularly those that created an empathetic connection.
There is an interesting story about iRobot and their product Romba. They reportedly spent years developing ways to prevent any empathy between the vacuum cleaner and its owners. And despite all their efforts in creating an impersonal robot they didn't succeed. This is a photo of my grandparents with their vacuum cleaner, they have named Bruce. And its not just that my family is strange, this is a tumblr user writing about their Romba they are comforting during the midst of a thunderstorm.

I really love this reply to this post:
Humans will pack bond with anything.

I wanted to explore this and unlike iRobot who actively try to avoid those empathetic connections I wanted to embrace them.

### Empathic Robots
So what does it mean to design robots that encourage the formation of empathetic bonds.
A key researcher working in this space is Christine Sunu. Sunu designs to purposely blur the lines between the alive and the unalive.
At her TEDx talk she showed off Furworm, a relatively simple robot that squeals and squirms the harder you squeeze it. As it wriggles in your hands, you feel empathy for it. You feel it struggling and respond irrationally and emotionally.
Through creating creatures like furworm, she has developed 3 features that can help create an illusion of life, and that connection between robot and human.  
They are purpose, emotion and story.
Purpose defines your creature's goals, wants, dreams and ambitions.
Emotional responses give a creature depth and personality.
And story contextualises the creatures in their environment. Where do they live, what do they do, how do they do.

In addition to these features I also looked at physical techniques based on the animation principles laid out by Disney.
Techniques such as easing in and out of motion, keeping responses dynamic, using a range of different methods of expression, designing motions and behaviours that are easy to understand all help a creature come to life.

Using these features and techniques as a foundation, I developed a framework that allows people to follow through steps and create their own empathetic creature.

## Validation
To validate this framework I gathered together a focus group of designers, educators, robotics enthusiasts, and asked them to dream up their own interactive robots.

We had a lot of fun coming up with all sorts of different creatures, each with different wants, needs and desires.

Through the focus group, and the creatures they created, I gained an understanding of how people imagine non-organic creatures looking, the spaces the might inhabit and how they might interact with people.


Each creature communicated its emotions differently ranging from changing skin colours to drooling.

After seeing the range of creatures and how people reacted a responded to them, calling them robots no longer seemed fitting. Therefore I am proposing they be referred to as non-organic creatures. Different from the organic animals we find today, but different from their robotic human serving counterparts.

## Design Criteria

From here I put together my design Criteria
* Firstly, I knew my main goal of this project was to provoke conversations and emotions about our relation to non-organic creatures. Therefore my design solution needed to incorporate the life
* Secondly, as I wanted to be provoking people, the solution needed to be design from the ground up with human interaction at the centre. Creatures would need to be durable, Highly responsive and consist of a range of materialities.
* Finally, I wanted the project to be accessible. To increase the impact of the creatures, a method of getting people to experience them will need to be included in the design solution. How, when, and where will people experience these creatures.
As part of making my project accessible, my design solution is open source. By releasing documentation of my own process and the technologies I've developed I want to contribute to the development of more of these non-organic creatures.

## Creatures
### All
Keeping this criteria in mind, I have designed a single creature and produced 3 stages of life cycle, an embryo, an adolescent and an adult, each built for interaction, and to form that empathetic connection each in their own way.

As inspiration for the forms of these creatures, I have looked to the ocean.
Ocean and deep sea creatures are unfamiliar, almost alien like.
I want to incorporate these feelings of other/alien into the creatures whilst still having them inspired and derived from something of our own earth.

### Embryo
The first stage in the creatures cycle is as an embryo.
The creature has its yellow brain in the centre of a squishy, slimy mass. This membrane protects the fragile developing bone structure and LEDs you can see wrapping around the edges.

Inline with my design criteria the embryo's form is designed to fit snuggly into cupped hands, inviting people to lift it up and interact with it. When raised from its resting place the embryo comes to life. The creature expresses its emotions through the LEDs and a faint but constant heartbeat. This heartbeat can only be felt by those holding the embryo making it quite a personal connection.

This creature has the most complex build process of the 3 I've designed. The skeleton is fabricated using SLA printing and held together using an acrylic plate. This plate also holds the circuit board brain and the electronics.
Using a CNC milled wax mould, I embed these components in a urethane rubber to protect it and give it that squishy slimy feel.

### Brain
At the heart of the all of the creatures is this yellow printed circuit board. This can be thought of as like the brain of the creatures. I have designed this board to make developing these interactive creatures easier.
It allows the creatures to interpret the world around them and respond appropriately. The board also allows to an embedded communication between each of the creatures so that they may react as a community to events.

This board is one of the many aspects of this project that can benefit others who want to create their own non-organic creatures.

### Adolescent
The second non-organic creature I have designed is the Adolescent. This creature is the developed form of the embryo who, having out lived its slimy membrane, has grown a soft crocheted skin.
This crocheted skin gives the creature its curled form with sensitive ribbing alongs its back, indents for where eyes should be and stubby tentacles on its end.
If you gently squeeze the skin you can feel the skeleton form just below the surface. A heartbeat pulses in the centre of the creature and LEDs give another method of expression.

I chose to use crochet for the construction of this creature as it's an easy to pick up skill, achievable with minimal equipment. Anyone could learn to crochet, follow my pattern, and have the form of their own creature  in a day.

To make arrangement of the electronics, I designed a skeleton insert. Made from flexible sheet polypropylene, the skeleton can be cut by hand and folded together to create the form. The electronics can then be sewn in and wired together. You can see this creature has the Creature Control Board at its heart, as well as a battery to allow a tether free experience.

### Tentacles
A stand out feature on this creature is these short stubby tentacles.
These tentacles use a unique mechanism I designed to achieve distinctly organic movement. The creature uses them as another method of expression, feeling out the world around it and twitching them if nervous.
The acrylic tentacles are covered with soft crocheted socks to keep the materiality consistent.

### Adult
The final non-organic creature I have designed is the Adult. This creature has a simple crocheted form, internal skeleton, LEDs for responding, and tentacles on its underbelly. This creature is a lot larger than the other two and is designed for embrace.

### Sensitive Skin
Both the adult and adolescent have conductive threads woven alongs it back giving it a sensitive skin. This sensitive skin allows for the creature to understand how it is being handled. Is it being gently stroked or hugged roughly? The creature can then respond accordingly.

## Exhibition
The reason I began exploring non-organic creatures, as I mentioned before, is because I am fascinated by the reactions and response they cause in humans.

As such these creatures that I have designed will sit within an exhibition open for people to come and experience non-organic creatures and what its like to empathetically connect with a creature.

The exhibition is designed to be sit within a shipping container and travel to reach a range of different communities targeting people with an interest in post-humanism, art, technology and robotics.

The walk through style exhibition is divided into two different areas. An informative section and an experiential section.

The informational section aims to introduce participants to the theory and thinking of non-organic creatures and prepare them to meet These creatures.
This will consist of information on the techniques used to design the creatures, artefacts such as bones, fossils and diagrams and examples of other non-organic creatures.
The participants are also given the opportunity to to create their own non-organic creature, using the techniques depicted in the exhibition.


Participants will then flow through into the second half of the container. This half is designed to mimic the habit of the creatures I have designed and feels like a zoo enclosure. Projections of water reflections light the room in an eerie blue, and rocky outcrops protrude from the wall.
Participants encounter the embryo first, as the are tethered to the rocky surface. They can reach out and cup one, feeling to its heartbeat. The embryos on the rocks respond in unison. If one is being mistreated they all react.
Moving on participants encounter the adolescent and adults, they are able to hold and interact with these creatures.

As the participants interact with the creatures, facilitators of the exhibition will assist in provoking conversations. Discussion about how and why these creatures we designed, raising questions with the participants about life, technology and the future of robotics.

The success of the exhibition as a whole relies on these conversations and can be measured through them.

<!-- ### Measuring success
To measure the success of the exhibition I've begun to think about some questions to ask at the
* Did they create their own non-organic creature?
* Did they interact with my creatures?
* Did they exhibition get them thinking about life and robotics?

Data indicating the success will be primarily measured by facilitators at the beginning and end of the exhibition. The facilitator is the last point of interaction after participants have walked through the exhibition. They gauge the experience of the participants, Do they look happy, are they deep in thought etc. In addition to this, logging of the participants interactions throughout the exhibition can assist in measuring success, ie - did they hold the creatures, did they linger around the embryo.

The success on the exhibition is dependent on how well it comunicates the future of these non-organic creatures. facilitators at the beginning and end will gauge the success through conversation with participents.
In addition to this, logging of the participants interactions can allows us to understand which sections are being most  -->

### Validation

To validate my design solution I reached out to Sarah Petkus, an artist and creature designer that I used in my research.
She said simply said that she loves my work and adores the design of the circuit board.

I also had the opportunity to take my creatures along to maker-faire wellington last weekend where I had a lot of fun showing off my creatures and seeing peoples reactions. Some surprised, some put off and some wanting to know how they could adopt their own.

### Wrap up

Through this project, I have learnt a great deal about the potential future of robotics and I hope to continue educating others through unique, interactive non-organic creatures.

Thanks
