---
onepage-id: "fp"
title: "Primary Research Methods"
nav-color: "sem1"
order: 34
---
### Research Questions

1. What technology is available for open source emotive robots?<br
 * **Secondary:**
Investigating existing open source projects to acquire an overview on the technology they implement.

2. What qualities contribute to a robot being perceived as alive?
 * **Secondary:**
Research the factors that define creatures as having life. Examine animation mechanics for motion, and expression.
 * **Primary:**
Perform a case study on a robot capable of feigning life. Complete evaluation sessions on my own designs.

3. How are emotions most effectively recreated in a non-organic creature?
 * **Secondary:**
Research Emotions and Emotion Theories. Research Emotional models and how researchers have implemented emotional models.
 * **Primary:**
Implement a number of emotional models and ask people to evaluate them.

4. How might emotive robots improve users quality of life?

5. What emotive products exist currently, and how do people relate to them?
 * **Secondary:**
Find existing products, and what reviewers, academics and the developers are saying.
 * **Primary:**
Interview people, asking them to evaluate the existing products.

### Possible Research Methods
34: Evaluative Research
"Evaluative research involves the testing of prototypes, products, or interfaces by real potential users of a system in design development."

44: Generative Designer
"Generative design exercises engage users in creative opportunity to express their feelings, dreams, needs, and desires, resulting in a rich information for concept development."

### Workshop
#### Generative Part
A generative task will allow participant to dream up their own non-organic creature, exploring the creatures role in their environment, and figuring out how they will react to differing situations.
From this will better understand the ecological neices that people can see these creatures existing.  
Where do you see a non-organic creature in your life?
* What is their purpose? What "ecological niches" do they fill?
* How do they express themselves?
* Create a story around them. (ie. what's their name, where did they come from, how did the get here.)

As part of the generative task, participants are invited to consider the form and aesthetic of their non-organic creature using a creative toolkit.
This will uncover what people expect non-organic creatures to look like. Everyone can imagine a robot but what should

#### Evaluative Part
I will show participants a non-organic creature I have designed and take note of immediate reactions, interactions, and feedback.

Show them some robots!
* How do people react?
* Is it how I expected?

#### Outcomes
To create a firmer idea of what people expect from non-organic creatures, which can be used to evaluate my creations.
I will be using this research to inform the function, behaviour, and aesthetic of non-organic creatures I will bring to life.

#### Trial workshop
I ran a small trial workshop with just 3 friends. The workshop was easy to run, sometime I had to give extra prompts about different each technique. Creatures didn't really focus on human interaction so I might bring this up.

#### People

{:.table}
Name|Role|Contact|Messaged?|Confirmed?
---|---|---|---|---
Wendy|Director of FabLab WGTN|Email||**Confirmed**
Matilde|FabLabWGTN Intern| || **Confirmed**
Sofia|Old work friend|FB|**Messaged** |**Confirmed**
Lucy|Robot enthusiast|FB|**Messaged** |**Confirmed**
Theo|CS Grad|FB|**Messaged** | **Confirmed**
Shanti|Journalist|FB|**Messaged** |**Declined**
Emily|VCD Grad|FB|**Messaged** |**Declined**
Emily|FabLabWGTN Intern|through Wendy|**Messaged** |**Declined**
Jonathan||||**Confirmed**
Indigo||||**HELPER**


#### Email to Wendy (FabLab WGTN)
##### Email 1
Good Morning, Wendy.

I hope you are enjoying a little bit of quiet over the break.
As I am sure you are aware the next step in my major project is primary research.
I am organising a Generative Design Workshop and would love to run it in FabLab WGTN.
For the bulk of the workshop, 4-5 participants will generate ideas for their own non-organic creature, thinking about the form, purpose, expression and story of the creature.
I am also planning on showing some creatures of my own that I create, seeing how participants react/interact with them, and taking feedback.

I'm hoping to organise this workshop for an evening either on Friday 26th, Monday 29th, or Tuesday 30th. The whole event should only be 1.5 - 2 hours.

Let me know if it's possible to hold the workshop in FabLab WGTN, and if you have any feedback on my plan.

Thanks,
Omelia

##### Email 2
Ki Ora, Wendy

Just sending an update about the workshop I'll be running on Tuesday.
It's pretty much all organised now. I've got 4 people coming as participants and 2 as facilitators.

I want to extend an invitation to participant to you as well as Emily and Daniel. I'd love for you all, if you are free, to participate as well.

Thanks

Omelia


#### Pitch

As part of my final year in Industrial design, I am researching techniques to blur the line between organic life and robotics.
I am organising a generative workshop and would love you to attend. Participates will be asked to generate ideas for non-organic creatures (robots), and review some existing designs.
The evening will be pretty relaxed and informal

It will be held at Fab Lab WGTN, Massey University at 6pm - 7:30pm Tuesday, 30th of April.

Let me know if you would like to attend.
Thanks,
Omelia

#### Postpone Message
Good Afternoon,
Sorry for the late notice, but due to falling sick over the past week, I have decided to postpone the workshop.
The workshop will now be held at 6pm - 7:30pm Thursday, 9th of May.
Sorry for the inconvenience, I'd still love you to attend if possible.

#### Reminder Message

Hi,

I hope you are as excited about the workshop tomorrow as I am. Below I've included some directions to fablabwgtn, some more information on the space and what we will be doing.

Fab Lab WGTN is inside block 11 at Massey University. The easiest way in is through the Fab Lab WGTN door on the west side of the building.
Find it on Google Maps: https://goo.gl/maps/9JHdmYRUromy7L1R9

Fab Lab WGTN have been kind enough to let me us their space. They have a brilliant code of conduct that they would appreciate you reading. https://fablabwgtn.co.nz/about/codeofconduct/

We will begin at 6pm and, if all goes to plan be finished up by 7:30pm. We will be doing lots of thinking, making and discussing so bring your creative hats. :)

Can't wait to see you there.
Omelia


#### Welcome

Introduce yourself,

talk briefly about my project and what im doing.

sign the consent forms

#### Warm Up Activity

Grab a piece of paper and some pens,
This is a warm up activity, we will be rapidly making a weird creature to help get our creative juices flowing.
You will draw one third of the creature then pass it on. So let's begin.

Start by choosing a habitat for your creature, Where are they going to live. Write that down then draw a head, thinking about all the things you creature needs and what they need, Do they have eyes, ears, hair?
Make sure you bring your creatures neck down below the line so the next person can continue your drawing.
Finally fold the paper down and pass it on.

Next, with out peaking at the previous persons drawing, draw the middle of your creature, think about how they grab stuff, arms, tentacles, something different. I also want you to write down what food they eat or a daily activity they do.

Fold the paper over and pass it on.

Draw the bottom of your creature, think about how they move around, do they walk, crawl, run etc.

Finally pass the creature once more and open it up. Take a minute to add any final details, think of a name, and a bit of a story. you are going to share this.


#### Explanation of Need Illusion
As I mentioned before my research project is about blurring the line between robotics and life. One way to do this is to forget about robots completely and focus instead on Non-organic creatures.


A small note: I want to focus on Creatures that have interactions with humans regularly.

Using a few key techniques we can create life like non-organic creatures. These techniques are Purpose, Emotion and story.


This worksheet will guide you through the creation of your creature.
So let's begin with purpose.

* **Purpose** define what your creatures goals are in their life. Ants find food, Rats steal food, Koalas climb trees to find food. Most organic creatures require food to live but for this exercise lets say our non organic do not need power. Find an ecological niche that your creature will fill.
* Second is **Emotion**.
 * Robots do but living things *want* to do.
 * How do they express themselves? A dog wags its tail, a cat meows in distress. How do they interact when they see people, when they recognise a person they know. Do they like loud or quiet environments.
* Finally, **Story**.
 * Think about where your creature comes from, the environment they in habit, how did they get here?
 * Story is about all the association we have to how the robot feels, looks, and sounds.
 * Create a story around them.
  * what's their name
  * where did they come from
  * how did  the get here
  * the environment they inhabit
  * What are their likes and dislikes.
 * Remember these aren't robots that humans have made, they are imagined non-organic creatures.

#### Generative Toolbox Supplies

Next up we will be creating a rough visual representation of our creatures. I got some materials.
* Tinfoil
* Pipe Cleaners
* Goggly Eyes
* Tape
* Play-dough

We will spend about 15 minutes doing this.

#### Evaluative Session

Introducing Fox.

Fox is a long lost lamb, abandoned by her traveling sheep family due to her small size. Because of this Fox has some serious attachment issues, when she senses the warmth of what could be her mother she will lurch for you, and hug you deeply.

So here she is. Have a hold, see what you think. Does she make you uncomfortable, or do you enjoy hugging her.


#### Wrap up

Thanks for coming.


### Low Risk Notification Form

###### Brief description (in lay terms) of the aim of the project, key research questions, and proposed research methods. What insights do you hope to generate through these methods? (approx. 200 words).

I want to explore technology that pushes and blurs the line between robotics and organic life to design open source emotive robots, and make them accessible to a wide range of everyday people.

My research questions are as follows:
1. What technology is available for open source emotive robots?
2. What qualities contribute to a robot being perceived as alive?
3. How are emotions most effectively communicated?
4. How might emotive robots improve users quality of life?
5. What emotive products exist currently, how do people relate to them and what are their downfalls?

Questions 1,2, and 5 can be answered mainly through secondary research methods, however to answer the more specific questions of emotive robots, how people relate to them, and how they fit into peoples lives, I think a primary research method based around talking to people interesting in social technology would be very valuable.

###### What type of people are the participants? (For example, occupation, age, gender, experience...)

As my project is targeted towards makers and DIY enthusiast, these will be the type of people I would like to interview. I, ideally, would have a range of ages, genders, and experiences within the maker/DIY communities.

###### How will participants will be identified and recruited?

I have good connections with the staff at Fab Lab WGTN, who intern are connected to a wide range of people from all over the world. Ideally, I could contact people that are involved with the fab lab movement via email.
I would also like to have input from people not connected to the fab lab movement, however I am currently unsure on how to find participants.

###### What will participants be asked to do?

At this stage, participants, after being briefed on my project and having given informed and voluntary consent, I would ask a series of questions related to my project. This data would remain confidential. I would also like the opportunity to ask follow up questions at a later date.

###### Please identify and discuss any potential problems: for example: medical/legal issues, issues with disclosure, conflict of interest, etc
