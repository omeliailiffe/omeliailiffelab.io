---
onepage-id: "fp"
title: "Design Brief"
description: "Version 1.0"
nav-color: "sem1"
order: 30
hidden: true
---

### Post-human Life: Open Source Emotive robots

#### Aim:
Through this project, I will utilise cheap and accessible technologies and processes to develop a robot that blurs the line between the robotic and organic life. I will explore differing methods of creating life-like attributes and explore potential issues related to emotive robotics.

#### Overview:
This project will explore aspects of life, usability and the relationship between a user (owner) and their robot.

The purpose of this project is not to create technology from scratch, but rather to build on those already developing robots, assemble technology suitable for life like robots and develop technics to further the illusion of life in robotics.

###### Aspirational projects

Christine Sunu

NoodleFeet

#### Brief

**Accessible Technology and Processes:**
Through out this project, I will be using technology (electronics, motors, materials etc) and processes (3d printing, laser cutting, sewing etc) that are accessible to a wide range of people through maker spaces and similar spaces.

**Aesthetics:**
Although at first tempting, the outcome of this project will not be a humanoid type robot. Many attempts at making humanoid robots fall short and end up in the uncanny valley. I believe avoid humanoid forms will push the project further and result in a more diverse and interesting outcome.

**Target Market:**
Those that want a gimmicky toy can turn to one of the many available robots on the market. This project aims to develop a product that can be adopted into the home, customised and made independent/diverse. The target market is therefore naturally tilted towards makers, DIY enthusiasts, and people looking for a project.

#### Design Criteria v1.0

**Physical Requirements:**
Easy to Assembly and use.
Customisable and unique appearance.
Uses Open Source, Accessible Technology and Processes.
A small form factor to allow for field trips outside of the home.
A soft, warm but durably body.

**Behaviour:**
Feign Life.
Dynamic.
Demand Empathy.
Interact and Respond to the user.










Primary User:

Secondary User:
