---
onepage-id: "fp"
title: "Initial thoughts and research"
nav-color: "sem1"
order: 1
---

### The Need Illusion
In 2018, I wrote an [essay](/assets/resources/post-human.pdf) (for Creative Culture and Ideas: Studies on post-human) discussing the presence of the artificial non-human entities that are becoming increasingly present in our day to day lives.
Through that essay, I explored a term Christine Sunu coined, **the need illusion**. Watch her Tedx talk below to get an introduction.
<div class="embed-responsive embed-responsive-16by9">
<iframe src="https://www.youtube.com/embed/xzUwsoqddjY" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Sunu, Christine. “The Need Illusion.” Youtube.com. TedxSoMa, 1 June 2018, San Francisco, California, www.youtube.com/watch?v=xzUwsoqddjY.
{:.caption}

> “**need** is the difference between a robot that deserves our sympathy and one we consider purely mechanical.” (Sunu 2018)

As Sunu discussed the need illusion is built upon 3 requirements, **purpose**: having consistency in actions, **emotions**: relaying emotions with a level of randomness, and **story**: associations and context of life.
I would like to keep exploring this idea/issue further through case studios and industrial designs.


### Open Source
A conclusion that I came to in my essay, was that if we wanted to avoid the potential harm of emotive robots, we can remove any profiting third party. This minimises the chances of exploitation. A potential way to do this is using open source technology and software.

Through-out my time studying at Massey University I have introduced to the idea of Open Source, and have involved the open source ideas and philosophy in my most memorable projects I've undertaken. Projects such as my open source midi controller, the interactive wall I designed for Fab Lab WGTN, and Greg, a beloved creature who loves to play.
With my final project I would love the opportunity to further explore the nature of Open Source, as a way of working and a mindset for the future.
