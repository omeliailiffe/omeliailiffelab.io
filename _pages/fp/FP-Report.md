---
onepage-id: "fp"
title: "Report"
description: "Post-human Life: Open Source Emotive robots"
nav-color: "sem1"
order: 40
hidden: true
---
Word Count: {{ page.content | number_of_words }}
### Table of Contents

* Overview
  * Introduction
  * Aim
  * Post-Humanism
  * Research Questions
* Context Review
  * Brief History
  * Trends
  * Social Robots
  * Life-Like Robots
    * The Need Illusion
    * Emotional Models
  * Case Study
    * NoodleFeet
  * Benefits, Downfalls and Opportunities for robotic companionship
* Technologies
* Research through making


People say it’s trying to replace people or pets, and we’re not trying to replace anything, of course, but it’s this new thing! It’s this new kind of relationship that can provide real value, not replace or compete.</br>
*Cynthia Breazeal, Professor and director of Personal Robots Group at MITs Media Lab*

### Overview
#### Introduction
In summer of 2018, a group of students including myself just finished creating an emotive robot. Named GREG, this robot, although originally designed as a platform for wearable applications, had become our toy, pet, and even friend. Baffled by what we had created, I went on to explore concepts around imbuing life into a non-organic organism.

#### Aims

Through this project, I will explore technologies and techniques that push and blur the line between organic life and robotics. I will examine methods of creating life-like attributes and explore potential issues related to emotive robotics. I goal is to create a series of robotics that, framed through post-human theory, take on a life of their own.
This report will document my exploration of different techniques, concepts, and technologies to assist with the blurring robotic design with organic life.
A secondary goal of this project is to open emotive robotic design up to a wide range of everyday people. As a result I will be focusing on cheap, easily accessible technologies, employing good documentation and making my designs open source.

### Post-Humanism
I have titled my project *Post-human Life: Open Source Emotive robots*. "Post-human theory is a generative tool to help us re-think the basic unit of reference for the human" (Braidotti 6).
<!-- Exploring the Post-human Theory in relation to large technological advances humans have made, I concluded that "they key to [...] understanding  the empathic connections we have with robots is understanding the post-human condition that frames the way we live in the Anthropocene." -->
Using the post-human theory we can begin to displace the emphasis we have given to ourselves as humans, and address the other forms of life that currently inhabit, and are yet to inhabit, this earth. One such form of life I would like to begin to develop is artificial, robotic life.


Can I even do this? As a human isn't it problematic to give myself the privilege of developing new forms of life in the name of Post-humanism!


### Research Questions

1. What technology is available for open source emotive robots?<br
 * **Secondary:**
Investigating existing open source projects to acquire an overview on the technology they implement.

2. What qualities contribute to a robot being perceived as alive?
 * **Secondary:**
Research the factors that define creatures as having life. Examine animation mechanics for motion, and expression.
 * **Primary:**
Perform a case study on a robot capable of feigning life. Complete evaluation sessions on my own designs.

3. How are emotions most effectively recreated in a non-organic creature?
 * **Secondary:**
Research Emotions and Emotion Theories. Research Emotional models and how researchers have implemented emotional models.
 * **Primary:**
Implement a number of emotional models and ask people to evaluate them.

4. How might emotive robots improve users quality of life?

5. What emotive products exist currently. How do people relate to them, and what are their downfalls?
 * **Secondary:**
Find existing products, and what reviewers, academics and the developers are saying.
 * **Primary:**
Interview people, asking them to evaluate the existing products.


### Context Review

#### Trend Map
{% include image.html id="trendmap" images="/assets/img/fp/trendmap.png" %}
#### Social Robots

##### OPSORO
"OPSORO stands for Open Platform For Social Robotics and is more than just a robot. OPSORO democratises social robot technologies and is meant for everyone. Not only techies, makers or developers, really everyone! Age or gender doesn’t matter." <small> [www.opsoro.com](http://www.opsoro.com/)</small>

OPSORO has been developed over a number of years, starting with a robot, ONO, and moving to a system that allows everyone to create their own social robot.

Although each robot is built on the same open source technology, their system creates an easy way for the user to create their own social robot skin. This means each robot can look and function unique.

>
<div class="row">
  <div class="col-4">
    <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Meet Walu, the Octopus Class Robot made by Claire Vandenameele and Yannick Stoelen. 🐙 He can change colors and kids adore him! <a href="https://twitter.com/hashtag/skins?src=hash&amp;ref_src=twsrc%5Etfw">#skins</a> <a href="https://twitter.com/hashtag/robot?src=hash&amp;ref_src=twsrc%5Etfw">#robot</a> <a href="https://t.co/mtpjAUIcVr">pic.twitter.com/mtpjAUIcVr</a></p>&mdash; opsoro (@opsoro) <a href="https://twitter.com/opsoro/status/883596648697536512?ref_src=twsrc%5Etfw">July 8, 2017</a></blockquote>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>
  <div class="col-4">
    <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Everyone: meet Gérard. He&#39;s a mix between a giraf and a robot and the most loyal friend a person could wish for! <a href="https://twitter.com/hashtag/skins?src=hash&amp;ref_src=twsrc%5Etfw">#skins</a> <a href="https://twitter.com/hashtag/social?src=hash&amp;ref_src=twsrc%5Etfw">#social</a> <a href="https://twitter.com/hashtag/robotics?src=hash&amp;ref_src=twsrc%5Etfw">#robotics</a> <a href="https://t.co/IRCOKlCGSw">pic.twitter.com/IRCOKlCGSw</a></p>&mdash; opsoro (@opsoro) <a href="https://twitter.com/opsoro/status/892701722778513409?ref_src=twsrc%5Etfw">August 2, 2017</a></blockquote>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>
</div>

##### Poppy-Project

The Poppy Project has created a number of different open source robots. Poppy Humanoid, a humanoid robot; Poppy Torso, similar to Poppy Humanoid but without legs; and Ergo Jr, a robot not dissimilar to the Pixar Lamp.
Their shared vision is that "robots are powerful tools to learn and be creative."
{% include image.html id="poppy" images="/assets/img/fp/poppy.jpg" %}
<small>[poppy-project.org](https://www.poppy-project.org/en/)</small>
##### Romibo

Designed to be a low-cost prototype kit to allow more social therapy research to be completed, Romibo is a small robot, who can move around and hold eye contact with children.
{% include image.html id="romibo" images="/assets/img/fp/romibo.jpg" %}

<small>[Webarchive](https://web.archive.org/web/20130531014635/http://www.romibo.org/therapy/)</small>



Whilst all of these robots are social through function many lack the techniques that allow a robot to pass into the realm of living and begin demanding empathy.

#### Life-Like Robots
##### The Need Illusion
Christine Sunu "designs successful, effective technology with emotive, human-centered interfaces." She created the furworm, and other emotive robots using a number of techniques she discusses in her TEDx talk.
Named the need illusion, these technique are method of giving a decisively un-alive robot a few key characteristics that draw people in and make them feel empathy towards itself. (Iliffe 5)
* Purpose <small>(consistency)</small>
* Emotion <small>(randomness)</small>
* Story <small>(associations)</small>

###### Purpose
Creating creatures that react consistently can give them the illusion that they have purpose, they have drive, they want to live therefore they must be alive.<br>
But consistence on its own is not enough.

###### Emotion
Robots do but living things *want* to do.
Simply adding joy and distress can bring a robot to life regardless of how mechanical it seems on the outside.<br>
Emotions that have staying power are unpredictable. Adding randomness to a robot can make seem much more alive.

###### Story
Finally, story is about all the association we have to how the robot feels, looks, and sounds.

Combining these techniques can create a compellingly realistic robot that has the ability to demand empathy from those it interacts with.
(Sunu, TEDx)


##### Emotional Models
Creating artificial, yet realistic emotions can be challenging. An emotional model can help connect sensory inputs with responses, adding consistency and randomness to better replicate the emotional function of a simple organism or even advanced, higher functioning emotions, such as a humans.

Arbib and Fellous discuss emotional models in their article, Emotions from Brain to Robot. They highlight a key interest in the development of these models as it allows for the imagining of a future where these models are not only enabling the simulation of emotional expression, but actually having emotions.


###### UNDERSTANDING EMOTION
Arbib and Fellous's article was written to collect and analyse the current state of emotional models in the science and computer science fields. They analyse the emotions, grouping them into 2 broad fields:
  "Emotional expression for communication and social coordination." and
  "Emotion for organisation of behaviour (action selection, attention and learning)."


The first concerns an external, key functions of emotion; To communicate simplified but high impact information. "A scream is extremely poor in information, but its impact on others is high."
(554)


Robot ethology (555)
ethology is the study of animal behavior

The second grouping focuses on ‘internal’ aspects.
Ortony et al. discuss the interplay of
four domains of functioning that contribute to an organisms effective **functioning**. "These domains are (a) affect – what the organism feels, (b) motivation –
what the organism needs and wants, (c) cognition – what it knows and believes, and (d) behaviour – what
it does." These domains, I believe, will be useful when designing robots, ensuring the designer is thinking about the robots place in the world and what ecological niche they will be birthed into.
Whilst in biology, the four Fs (feeding, fighting, fleeing and reproduction) occur, and over time evolution will cause creatures will fall into these functions, these cannot be easily applied to robotics. "Robot design will normally be based on the availability of a reliable power supply, and the reproduction of robots will be left to factories." Therefore, Robots will instead rely on their key tasks to inform and define their "ecological niches" (Arbib and Fellous 559).

The four domains (affect, motivation, cognition and behaviour) laid out by Ortony et al. show that changes to an organisms emotions, caused by affects and influenced by motivations, changes the operating characteristics of cognition and action selection (Arbib and Fellous 559). A creature will react to differently to situations and make different decisions when experiencing a range of emotions. A system that further assists in the emotional modelling of an organism is a "cognitive architecture".
A "cognitive architecture" creates multiple levels for which the role of emotions can be placed (Arbib and Fellous 555). Ortony et al.'s proposed "cognitive architecture" includes the levels Reactive, Routine and Reflective. Each is responsible for different cognitive levels of thinking.
Arbib and Fellous summarise Ortony et al.'s levels:
  (1) Reactive: a hard-wired releaser of fixed action patterns and an interrupt generator. This level has only the most rudimentary affect.
  (2) Routine: the locus of unconscious well-learned automatised activity and primitive and unconscious emotions.
  (3) Reflective: the home of higher-order cognitive functions, including metacognition, consciousness, self- reflection, and full-fledged emotions.

"Ortony et al. address the design of emotions in computational agents (these include ‘softbots’ as well as embodied robots) that must perform unanticipated tasks in unpredictable environments. They argue that such agents, if they are to function effectively, must be endowed with curiosity and expectations, and a ‘sense of self’ that reflects parameter settings that govern the agent’s functioning."

###### A different model



###### IMPLEMENTING AN EMOTIONAL MODEL
Ribeiro and Paiva discuss a few different emotional computational models and go on to explain how they implemented on for their own robot (385, 387).




##### Organic Movement

The challenge of making robotics based organisms believable, emotional, desirable figures shares many similarities with the development of on screen, animated characters, as pointed out by Ribeiro and Paiva. In their article, The Illusion of Robotic Life, Principles and Practices of Animation for Robots, they highlight a set of principles developed over many years and compiled by Johnston and Thomas (two animators part of a group who worked closely with some of Walt Disney's first and most famous animation works) (383). Ribeiro and Paiva examined each of the 12 principles and presented them, adding how they relate to the animation, and creation, of robotics (384).

###### Summary
Movement - Straight Ahead and Pose-to-Pose, Follow-Through and Overlapping Action, Slow In and Slow Out, Arcs.

When moving with either procedural animation() or predesigned animation(), creatures should move with anticipation, follow-through, and animations should blend softly together (slow in and slow out). When horizontally, movements should include some vertical movement as most natural motions. happen in arcs.

Behaviour - Staging, Secondary Action, Timing, Exaggeration, Solid Drawing, Appeal.

Creatures should use a range of expressions, and expression methods, ie sound and lighting (staging). When performing actions, secondary actions naturally occur, ie blinking, breathing, scratching. Timing can be used to convey an emotional state. When design poses and motions, avoid symmetry if possible, as it looks unnatural. To make a creature appealing, motions and behaviours should be easy to understand, exaggerating motions can help the understandability.

###### Staging
Staging is related to the general set-up in which the character expresses itself. In robots this suggests that we can use multi-modal expression with lights or sound.
###### Straight Ahead and Pose-to-Pose
We can regard this as having a robot to produce interactive, procedural animation (straight-ahead), or predesigned animation, which can be synced and blended with other kinds of behaviors (Pose-to-Pose).
###### Follow-Through and Overlapping Action
A character that punches another one will first pull its body and arm back (anticipation), then punch (action), and slightly fall forward while trying to regain balance, give step or two or even fall down (follow- through/reaction). These principles have impact both on causing the impression that the robot is part of our natural world, and also to mark that an action has ended.
###### Slow In and Slow Out
This principle helps the motion of objects and characters to seem natural and pleasant to the viewers. Along with anticipation and follow-through, objects shouldn’t be abrupt when they start or stop moving. A hand pulling back for a punch accelerates backwards, decelerates to a stop, and then accelerates forward to the punch.
In robot animation this principle is one of the major ones to apply, as it states that animations should softly blend one into another, or in and out of the character.
###### Arcs
This principle states that natural motions occur in arcs. When a robot looks to the left and the right, it shouldn’t just perform a horizontal movement, but also some vertical movement, so that the head will be pointing slightly upwards or downwards than it was while facing straight ahead.
###### Secondary Action
When people speak to each other, they often scratch some part of their body, adjust their hair, or even look away from the person with who they are interacting. These actions are designated as secondary actions. One simple case of applying this to robots can be adding random blinks to the eyes, and adding a soft, slow sinusoidal motion to the body to simulate breathing.
###### Timing
Timing of physical motion on Earth or on the Moon is very different. But timing can also be used as expression. A fast motion often suggests that a character is active and engaged on what it’s doing.
For robots the scalability factor of time is interesting to explore, as the same movement, pre-animated or procedural, can have different meanings depending on the timing used, which aids on reusing motions and animations with different emotions.

###### Exaggeration
This principle, along with Timing, is one of they key magic features in animation. Exaggeration can be used to emphasize the robot’s movements, expressions or actions, in order to make them more noticeable and convincing.
###### Solid Drawing
This principle states some rules to follow while designing poses. A character should not stand stiff and still. The main concept to get from this principle is asymmetry. Faces rarely exhibit the same expression on the left and right sides, and almost no poses are symmetrical, unless one wants to convey the feeling of stiffness.
###### Appeal
Motion and behaviors of robots should also be easy to understand, because if the users don’t understand what they see, their appeal for the robot will fall.



#### Case Study: NoodleFeet
NoodleFeet is a young robot, created by the artist, Sarah Petkus, Petkus began creating Noodle in 2015 and over time found she was referring to Noodle as her 'child' or 'offspring' and referring to herself as 'mother'. This way of thinking has become critical to the designs around how Noodle develops. Noodle is no longer just a robot but has wants, ambitions and dreams. As Noodle acquires new hardware and software updates, these changes are thought of as 'growing'.

Petkus, in her artist statement writes, "I think the way we relate to our technology must shift and evolve as we continue to innovate. Whether a creation serves us in a utilitarian way, or acts as a unique extension of the individual who created it, we should continuously challenge where we draw hard lines between the two, and where what we create overlaps with our sense of self."

{% include image.html id="noodlefeet-beach" images="/assets/img/fp/noodlefeet3.png" %}
<small>[noodlefeet.zoness.com/](http://noodlefeet.zoness.com/)</small>

NoodleFeet is an interesting robot as his life-like qualities do not come from being cute, fluffy or [], but, due to Noodle's infancy, come, almost entirely, from the **story** surrounding him and from his relationship with his mother. Although Noodle is just learning to walk, he does have some other skills. He has learnt to recognise his mother, Sarah, and a number of his favourite stuffies.
Noodle also has 2 "emoting eyes" which Petkus says are the things you look into to establish that Noodle has a soul.

#### Technologies <small>cheap, open source</small>
There are a lot of technologies to help create well crafted robots with life-like qualities.
These have been spilt into different categories, each relating to a different ability. Controllers: micro-controllers. Inputs: Seeing, Hearing, Touching. And outputs: Movement, Light, Sound.
##### Inputs
Being able to react to the environment around themselves is a critical part of being alive, and in order to do this robots need sensors.
###### Seeing
Vision comes in a range of different scopes.
Firstly, adding a camera could allow your robot to recognise faces, objects and environments but takes a lot of processing power.
Another way of seeing is using an ultrasonic distance sensor. This sensor allows the robot to detect how far away a wall or object is. This can be used to prevent your robot from running into walls repeatedly and unlike a full camera, it doesn't need a lot of processing power.
Finally, an even simpler form of vision is a light sensor. This sensor send a low value when in darkness and could be utilised cleverly to detect people, walls and other objects.
###### Hearing
A hearing ability can allow a robot to turn to whomever is speaking, startle at a loud sound, or even respond to commands.
Microphones are connected to a digital pin and can be programmed to recognise intensities of sounds.

Implementing a speech to text program is much more complicated but can be done with a powerful micro-controller (such as a raspberry pi).

###### Touching
Detecting touch is an important feature, especially when interacting with people.
One simple non-intrusive way to achieve this is through capacitive touch sensors. Using a capacitive touch sensor, conductive materials, such as adhesive copper film or conductive thread, can used to detect when a person touches your robots arm, soft fur, or other conductive surface.

##### Outputs
###### Movement
Movement is created through the use of Motors or Solenoids.
When creating movement for robotics it is important to use controllable motors; Such motors are able to move to certain positions at different speeds and accelerations.
As Ribeiro and Paiva explained, "objects shouldn’t be abrupt when they start or stop moving" but should ease in and out of movement. To do this controllable motors are needed.

The 2 main types of controllable motors are Servo motors and Stepper motors.

###### Light
Light can be one of the easiest things to add to a project. LEDs (Light Emitting Diodes) can be turned on and off with a digital pin, however will only be the one colour. RGB LEDs can express a full range of colours but can be hard to control.
The RGB WS2812B LEDs make it easy. These LEDs come in many form factors, can be chained together (controlled through on one digital pin), and have a wide level of support in the open source community.

I recommend using FastLED (a library for Arduino), which makes controlling your LEDs a breeze.

###### Sound
Adding simple sounds to a project can make it seem much more alive. Using a cheap 8ohm speaker (available from the fab inventory or hobby stores) and a digital pin will allow you to create single tone beeps and blips.
To add a wider range of sounds, a Digital to Analog (DAC) convertor is needed. This device will connect to a controller and convert a digital audio stream into an analog audio signal.

Something to note is audio files tend to take up a lot of storage space, so choosing a micro controller with lots of storage or adding an SD card is a good idea.


### Primary Research

Generative Design Workshop
Through a generative design workshop, I will gain a better understanding of
Through the form of a workshop, I have engaged in 2 primary research methods
Firstly, in order to further understand how people see the role of non-organic creatures in our world, I will run a generative workshop.

The areas explored through primary research are:

how to best generate situations and locations for non-organic creatures to thrive in.

Further understanding peoples expectations surrounding non-organic creatures; where they exist, how they look and function, and how they interact with people?

Gain insight into what people respond to when interacting with non-organic creatures? What do they tolerate, enjoy and desire from creatures they interact with?

#### Generative Research Activity
##### Aims
To understand how people see the role of non-organic creatures in our world.




##### Description
A generative design workshop, invites participants to explore non-organic creatures creativity through thinking and making. After a short creative warm-up task, participants are guided through creating their own non-organic creature, using the need illusion as a framework.
After spending time thinking about a creatures purpose, how they express their emotions and other associations, and details about the creature, participants are invited to explore the physical form of their creature using rapid prototyping materials.
##### Results
Although I am yet to complete my workshop, I did run a small scale test workshop.

The creatures that the participants designed were varied and unique. Built upon a fundamental purpose (ie, find water), they were unlike any life currently found.

A creature that was particularly interesting is _____

Through analysing the results, I am reminded that the people I choose as participants is important. I need creative minded people that are willing to explore the ideas presented and develop creatures with depth.

Another thing I have learnt is that I want to focus more on human encountering creatures. Creatures that live in urban environments and/or have interactions with humans regularly. Participants will have to think more about how their creatures will respond to humans.

#### Creative Toolkit
##### Aim
To explore a visual language for non-organic creatures, inspired not by robotics but by biomimicry.

##### Description
After completing the generative portion of the workshop, participants are invited to begin exploring the form and aesthetic of their creature. Through the use of materials like playdough, lego, paper, tape, crayons, etc participants will rapidly express a rough idea of what their creature might look life.

##### Results
Unfortunately, due to a lack of supplies I couldn't test this activities when running the test workshop last week. Instead, I asked participants to sketch their creatures.

With 3 participants, Participant 1 drew a creature inspired by a butterfly, whilst Participant 2's creature was much more abstract (their design was less informed by existing animals).


#### Evaluative Workshop

##### Aims
To track my progress on current emotive non-organic creature designs.
To test different methods of communicating emotions.

##### Description
An Evaluative workshop seeks to put non-organic creatures in front of participants and document their reactions, interactions, and experiences.

Data collection:
keyword collections?
have a form to fill out? how did you find interacting with this creature.

##### Results

As this workshop is run together with the generative design portion I have not had the opportunity to complete it.
From the workshop, I hope to

#### Design Crit

#### Concept 1: An experience based Installation

*
* Illustration showing the space
* Fox on display

This first concept focuses heavily on the experiences of encountering non-organic creatures. It aims to push peoples comfort levels, provoke conversation, and raise emotional responses to the creatures.
The concept involves the development of a range of non-organic creatures with human centered purposes, and supporting material to facilitate the experience.

Target Audience:

#### Concept 2: An open source, diy kit to make your own non-organic creature

* A take-away for people looking at my mini-presentation
* An example kit and documentation, to build _____
* An example webpage?

This concept focuses more on the distribution of the techniques and technologies highlighted in this report. It strives to open up to world of non-organic creatures to the open source community, and hopefully inspire the creation of new, unique creatures.

The concept involves the creation of a modular system of parts that can be created using accessible tools, and assembled in many unique ways to create a creature. In addition to the physical system, a platform for guides, instructional videos, generators, worksheets and other tool useful for the creation of creatures would also be designed.

#### Concept 3: Something more Product designy

* Singular desirable commercially released

This concept focuses on designing a single well-marketed, mass-producible product.
The product would allow a range of consumers to access the emotive technology embedded in a creature. The product allows for a customisation in the creatures purpose, allowing a wide range of uses.
