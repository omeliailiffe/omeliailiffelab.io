---
onepage-id: "fp"
title: "Primary Research Presentation"
nav-color: "sem1"
order: 35
hidden: true
---

Things to include in our presentations
* Context
* Participants
* Method
* Outcomes


### Context

I am looking for techniques and technologies that blur the line between organic life and robotics.
This project is rooted in post-human theory. Post-human theory is used to displace the emphasis that we place on human life over other forms, and types of life. One such life, often dismissed, is robotic life.
Through this project, I want to raise questions about life robotics and non-organic creatures, challenging people's perceptions and making people uncomfortable.

### Areas of secondary research
Through secondary research I have been looking at a range things. Existing projects, such as noodlefeet, and the furworm.
Exploring techniques, like the need illusion utilised by burbles.
or research how to implement complicated emotional models which take how we know emotions to work in human brains and emulate them on a micro-controller.

### Primary Research
There are a couple of areas where primary research can push my project forward. Firstly, generating ideas for locations and situations in which non-organic creatures can not only exist but thrive.
and secondly, what people respond to, what will the tolerate from a non-organic creature and what will they really enjoy.


### Methods
I will be running a workshop next week, and have selected a number of creative and/or techy minded people to attend. Together we will go through a few different activities.

A generative task will allow participant to dream up their own non-organic creature, exploring the creatures role in their environment, and figuring out how they will react to differing situations.
From this will better understand the ecological neices that people can see these creatures existing.

As part of the generative task, participants are invited to consider the form and aesthetic of their non-organic creature using a creative toolkit.
This will uncover what people expect non-organic creatures to look like. Everyone can imagine a robot but what should

And finally, an evaluative session, where I show participants a non-organic creature I have designed and take note of immediate reactions, interactions, and feedback.

I will be using this research to inform the function, behaviour, and aesthetic of non-organic creatures I will bring to life.
