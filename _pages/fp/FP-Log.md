---
onepage-id: "fp"
title: "Interaction Log"
nav-color: "primary"
order: 99
---

### Week 1
**Lyn**: Crowdsourcing data for your exhibition could be really interesting
**Wendy**: Look at museums!

### Week 2
**Lyn**: P
**Myself**: Look at

### Week 7
**Lyn**: Explore the context of the creatures, looking at the exhibition, how people interact and learn in the space.

### Week 8
**Lyn**:
How do we measure the success?
whats the goal?
how do we evaluate the exhibition?
takeaway?

What do the creatures do? What is their ecological niche?

library communities spaces etc

ask "maybe next year diversion
"

electrical bioluminescence
different modes of swimming
*

## Semester 1
### Week 1
##### Wednesday (27/02)

**Emma**: Explore existing things such as the companion for autistic children (Victoria University), Helen Andre.
Perhaps look at living dolls (not so much the ones of a sexual nature) and baby dolls. Look at emotive robots designed for well being.

#### Friday (1/03)
##### Lecture
**Lyn**: Trend Maps (Bipolar Graphs) <br/>
**Rod**: Plagiarism and Referencing

**Wendy**: Take micro-breaks :)

**Lyn**: You could take a narrative approach to these project, but I feel there is something more there.
Flip your topic proposal round so it becomes a technology first project. Explore the technology to ultimately end up with a product.

## Week 2
#### Monday (04/03)
**Lyn**: Lyn talked on research methods and ethics. He recommended buying a couple of key books.

#### Tuesday (05/03)

**Elizebeth**: When having trouble with sketching... Write a blurb of what you are trying to achieve with your sketches and then keep trying to achieve that. Show the sketches to others and see what they can infer and if it is close to your blurb.

## Week 4
#### Friday (22/03)

**Lyn**: Lyn and I discussed the research I had done and the potential direction of my project. Some questions that were posed to me:
* What do you see your exhibition space looking like:
    Robots; Lots of cool robots
* What form of primary research would I like to do:
Lyn also suggested I need more focus.
## Week 5
#### Monday (25/03)

**Wendy**: I spoke to Wendy this morning, giving her an overview on my project basis, the research I had explored and the robot I quickly made (Guppy).
She had some interesting things to say. We discussed why I have doing this research which has made me do a lot of thinking. Why am a building robots that feign life and demand empathy.
Is it to raise question about life and post-humanism in those who interact with my robots (to educate);
Is it to develop a new method of creating robots;
Is it to give objects a self defensive system against vandalism;
Is it to create a replacement for pets in the home;
Is it to create a new way of communication for long distance relationships.
Is it to challenge the idea of gender through applying gender to otherwise non gendered robots;

#### Friday (29/03)

**Lyn**: I showed Lyn my robot, Guppy and we chatted about where my report is up too. I need to add more visual analysis (not just images but trendmaps, diagrams and other analysis things). I also need to start pointing out why the research is valuable to my topic. How will it influence and be useful.
Also begin adding my things to an indesign document.



## Week 7
#### Friday (12/04)

**Lyn**:
secondary research: little bit more linking and stuff

primary research: Generative workshop sounds good. Networking to get a range of people involved.

## Week 8
#### Friday (03/05)
##### Primary Research Presentation
**Rod**:
Great presentation, I don't really get it or understand it. But I'm excited to see where it goes.

## Week 10
#### Friday (17/05)
##### 3 Concept Presentation
Group gave not very helpful feedback.

## Week 11
#### Friday (24/05)

## Week 12
#### Friday (31/05)
##### Final Presentation Feedback
**Lyn**:
Definitely needed to have your 3 concepts and evaluation in
Talk more about why you chose this project. Bring more of an emotion connection in to your presentation.
