---
onepage-id: "fp"
title: "Summaries"
nav-color: "sem2"
order: 40
---
## Concepts Presentation Poster
### OPEN SOURCE NON-ORGANIC CREATURES
I am creating an exhibition, inviting people to experience a family of non-organic creatures. These creatures, unlike robotics, take on a life of their own by employing techniques, correctly implemented technologies, and by being framed through a post-human lens.
#### Form
Ocean and deep sea creatures are often unfamiliar, and almost alien like. I want to incorporate these feelings of other/alien into the creatures whilst still having them inspired and derived from something of our own earth.
#### Materials and construction
I am exploring a number of different construction techniques (currently sewing patterns, and crochet) and will be selecting a technique using my design criteria.
#### Exhibition
I am designing an exhibition, inviting people to come, experience the illusion of life in the creatures I create and provoke conversations about life, robotics, and the growing presence of non-organic creatures. The exhibition will be highly interactive, allowing the creatures to engage with the participants.
As developed currently, the exhibition consisted of three components, a crowd-sourcing station, a display of artefacts, and the interactive component (a menagerie)
#### Crowd sourcing
A large portion of my research was compiling a framework that allowed people to think about and create their own non-organic creatures. A natural progression to this work is to make it more accessible. I am looking at ways to distribute the framework, and collect crowd-sourced data of non-organic creatures.

## State Of Play Presentation Poster
### OPEN SOURCE NON-ORGANIC CREATURES
In our current world, robot are created with human centered purposes, designed to for-fill simple tasks delegated to them by humans.
As technology, and the accessibility of technology, develops a new domain of life will be created.
The creatures in this domain (previously thought of as robots) are not human centered, but exist in their own ecological niches, seeking not to achieve the goals of humans but their own wants, dreams, and ambitions.
These special class of animals are call Non-organic Creatures.

I am working in this future space, drawing inspiration from deep sea life, to develop non-organic creatures that provoke conversations about life, robotics, and the looming reality of non-organic creatures.

Please feel free to engage with the creatures I have designed.
