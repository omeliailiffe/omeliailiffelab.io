---
onepage-id: "fp"
title: "Making"
nav-color: "sem2"
order: 40
---

#### Non-Organic Creatures
##### Guppy
Introducing Guppy
{% include image.html id="guppy-1" images="/assets/img/fp/guppy1.jpg" %}
Guppy is very much in her infancy; She can move her legs and chirps occasionally but currently struggles to interpret the world around her.
##### Fox
{% include image.html id="fox1" images="/assets/img/fp/fox1.jpg" %}
Fox is a lovable non-organic creature, abandoned at birth by her family of roaming, high-altitude sheep on the Himalayas. After being rescued she was brought to New Zealand to try to be re-introduced to a flock.
Due to her harsh abandonment, Fox craves attention and loves warm hugs. She squirms when you pet her and when held close to a warm body she squeezes you tightly.
She has been developed using tentacle mechanisms for arms, a micro-controller brain, and temperature sensing underbelly.


#### Soft Toy Construction
##### First prototype (sewing)
https://www.craftster.org/forum/index.php?topic=281729.msg3190856

#### Crochet
##### An Octopus type pattern
http://avtanski.net/projects/crochet/lathe/cgi-bin/lathe.cgi?data=11,0.75,1,15,8.5,1,31.25,9.75,1,35,13,0&pname=&scale=3

```
Start with:        10 st in a loop
Row   1 [ 16 st]:  inc, 1, inc, 1, inc, inc, 1, inc, 1, inc
Row   2 [ 23 st]:  1, inc, 2, inc, 1, inc, 1, inc, 2, inc, 1, inc, 1, inc
Row   3 [ 29 st]:  inc, 2, inc, 3, inc, 3, inc, 3, inc, 3, inc, 3
Row   4 [ 35 st]:  1, inc, 3, inc, 4, inc, 4, inc, 4, inc, 4, inc, 3
Row   5 [ 41 st]:  3, inc, 4, inc, 5, inc, 5, inc, 5, inc, 5, inc, 2
Row   6 [ 47 st]:  6, inc, 5, inc, 6, inc, 6, inc, 6, inc, 6, inc
Row   7 [ 53 st]:  1, inc, 7, inc, 6, inc, 7, inc, 7, inc, 7, inc, 6
Row   8 [ 54 st]:  42, inc, 10
Row   9 [ 54 st]:  54
Row  10 [ 55 st]:  15, inc, 38
Row  11 [ 56 st]:  43, inc, 11
Row  12 [ 56 st]:  56
Row  13 [ 57 st]:  15, inc, 40
Row  14 [ 57 st]:  57
Row  15 [ 58 st]:  43, inc, 13
Row  16 [ 58 st]:  58
Row  17 [ 59 st]:  14, inc, 43
Row  18 [ 59 st]:  59
Row  19 [ 60 st]:  44, inc, 14
Row  20 [ 60 st]:  60
Row  21 [ 65 st]:  2, inc, 11, inc, 11, inc, 11, inc, 11, inc, 9
Row  22 [ 70 st]:  4, inc, 12, inc, 12, inc, 12, inc, 12, inc, 8
Row  23 [ 74 st]:  13, inc, 16, inc, 17, inc, 16, inc, 4
Row  24 [ 79 st]:  13, inc, 14, inc, 14, inc, 14, inc, 13, inc, 1
Row  25 [ 81 st]:  13, inc, 39, inc, 25
```



Abbreviations:
```
sc: single crochet
ch: chain
2 sc in next sc: increase
* *: repeat step
F/O: fasten off
```
Special stitches:
```
 Shell: skip 2sc, *dc in next sc*, repeat 6 times for 7 dc in sc total, skip 2sc, sc in next sc
```

Body (worked top down):
This pattern is worked in joined rounds.
```
1. ch 5, join
2. 2 sc in each sc around [10]
3. *sc 1, 2 sc in next sc*, repeat around [15]
4. 2 sc in sc,  *sc 1, 2 sc in next sc*, repeat around [23]
5. sc 3, *sc 3, 2 sc in next sc*, repeat around [28]
6. *sc 6, 2 sc in next sc*, repeat around [32]
7. *sc 7, 2 sc in next sc*, repeat around [36]
8. *sc 5, 2 sc in next sc*, repeat around
(This is as wide as I wanted mine to be, if you want yours wider keep making increase rows.)
9-25(?). sc around
(How many rows you do here depends on how tall you want it to be. I don't remember how many I did; I just kept crocheting until it was about 9cm high!)
26. *shell*, repeat around
F/O, weave in end.
```


##### Basic Amigurumi Ball Pattern
```
8 sc in magic ring
Round 1: inc in every other (12)
Round 2: inc in every other (18)
Round 3: inc in every 3rd (24)
Round 4: inc in every 3rd (32)
Round 5: inc in every 4th (40)
Round 6: inc in every 5th (50)
Rounds 7 – 11: sc around for 5 rounds (50)
Cut yarn and attach contrasting color.
Round 12: sc around 1 round (50)
Cut yarn and attach main color.
Rounds 13 – 17: sc around for 5 rounds (50)
(Now it’s time to practice that invisible decrease!)
Round 18: dec in every 4th (40)
Round 19: dec in every 4th (32)
Round 20: dec in every 3rd (24)
Start to stuff with scrap yarn or fiberfill.

Round 21: dec in every 3rd (18)
Round 22: dec in every other (12)
Round 23: dec in every other (8)
Cut yarn and secure.  Weave in yarn ends.
```

#### Resins
|Resin|Hardness|Colour|Link|Notes|
|---|---|---|---|---|
Transil - Barnes |20A|White translucent| https://www.barnesnz.co.nz/addition-cured/transil-translucent-silicone-rubber-2288#/2-sizes-1kg
F150 POLYURETHANE RESIN - Barnes |50A Amber translucent |https://www.barnesnz.co.nz/polyurethane/f150-2030
WC565 WATER CLEAR POLYURETHANE RESIN-Barnes|65A |Water Clear |https://www.barnesnz.co.nz/polyurethane/wc565-1440| Really expensive ($220)

#### Coding
##### MeshNetwork
The creatures will be able to communicate to each other (and with the exhibition sensors).  
An efficient way to do this is with a [mesh network](https://gitlab.com/painlessMesh/painlessMesh).
###### First Test
My first test used the example code and 4 wemos d1 mini's (esp8266 based micro-controllers).  
The micro-controllers were able to connect in a mesh and send messages to each other.
This will be perfect for what I am needing.

##### Test Setup
Whilst the pcbs I have designed are getting fabricated I need a test setup of electronics to test/code with.
This will include
wemos lolin32 lite
mpu6050 (gyro/accelerometer)
mpr121 (capacitive touch)
ws2812 led strips
speaker
