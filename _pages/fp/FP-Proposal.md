---
onepage-id: "fp"
title: "Topic Proposal"
nav-color: "sem1"
order: 20
hidden: true
---


### Post-human Life: Open Source Emotive Companions

#### Elevator Pitch
I want to design open source emotive companions that offer everyday people an alternative to closed source robots which collect information, and have the potential to exploitive their users.

#### Topic Description
With the rise of consumer electronics, there has been an increase in robots in our lives. This has been highlighted as an issue as we begin to perceive them as alive and empathise with them. This flaw in ourselves can potential be exploited by manufacturers and companies who design emotive robots. Christine Sunu (a key inspiration of mine) designs robot that purposely explore this boundary and blur the lines between robot and life. A potential solution proposed by Sunu, is to remove the third party (and therefore the risk of exploitation) and design these emotive companions ourselves. As building emotive companions is not achievable by everyone, I want to work on a middle ground: Designing Open Source Emotive Companions for Everyone.

#### Keywords
 * Emotive Companion
 * Robots
 * Open Source

#### Why did you choose this Topic?
In 2018, I wrote an essay (for Creative Culture and Ideas: Studies on post-human) discussing the presence of the artificial non-human entities that are becoming increasingly present in our day to day lives. I had also just designed a prototype wearable pet and both of these captured my interest. I haven't been able to stop thinking about it since.
#### What interests you about the topic?
I was introduced to open source design by Fab Lab WGTN and see it as the obvious way forward. I see open source design as a potential solution and opportunity for the issue I identified above, which really excited me.
#### How this topic will help your design career?
Being successful in the open source world, means being visible and having a following. I would like to tackle a large project such as this to launch my open source presence online and around the fab network.
