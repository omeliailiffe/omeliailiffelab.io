---
onepage-id: "fp"
title: "External Validation"
nav-color: "sem2"
order: 42
---
### Sarah Petkus

Hi Sarah,
My name is Omelia Iliffe, and I am studying Industrial Design at Massey University, New Zealand.
I've been working on my final project: making interactive robotic creatures, and have used your work with NoodleFeet as inspiration and reference throughout the past year.
I have designed a number of highly responsive, interactive creatures that act as we would expect an animal to act. When hold them you can feel their tentacles squirm and their heartbeat pulse.
I would love for you to give me some feedback on the creatures that I have designed.
What do you think is successful, or done well?
Please feel free to ask questions and any comments would be greatly appreciated.
Thanks heaps,
Omelia

### Christine Sunu

Hi Christine,
My name is Omelia Iliffe, and I am studying Industrial Design at Massey University, New Zealand.
I've been working on my final project: making interactive robotic creatures, and have used your work (the need illusion, burbles, HaRoCo etc) as inspiration and reference throughout the past year.
I have designed a number of highly responsive, interactive creatures that act as we would expect an animal to act. When hold them you can feel their tentacles squirm and their heartbeat pulse.
I would love for you to give me some feedback on the creatures that I have designed.
What do you think is successful, or done well?
Please feel free to ask questions and any comments would be greatly appreciated.
Thanks heaps,
Omelia
### /r/robots

For the past year I've been developing, refining, and implementing techniques to make robots more life-like.
Above is one example of the highly responsive, interactive creatures I have designed. When hold them you can feel their tentacles squirm and their heartbeat pulse. These creatures are all based around an open source (Creature Control Board)[https://github.com/harryiliffe/creature-control-board] I designed.
I would love some feedback and to answer any questions.

### /r/esp32 & /r/opensource & /r/electronics
#### Open Source Interactive Creatures (See comments for link)

For the past year I've been developing, refining, and implementing techniques to make robots more life-like.

Above is one example of the highly responsive, interactive [creatures](https://imgur.com/gallery/1HTYAHN) I have designed. When hold them you can feel their tentacles squirm and their heartbeat pulse.

These creatures are all based around an open source Creature Control Board I designed.It is based around an ESP32 and numerous ICs such as:

* capacitive touch,
* gyroscope and accelerometer,
* an amplifier,
* and onboard lipo charge circuit.

[You can find the PCB design here.](https://github.com/harryiliffe/creature-control-board) Version 2 is coming soon as V1 has some issues.

I would love some feedback and to answer any questions.


### Sarah's discord
Hi all, long time lurker of the discord.
For the past year I've been developing, refining, and implementing techniques to make robots more life-like.
Above is one example of the highly responsive, interactive creatures I have designed.
When hold them you can feel their tentacles squirm and their heartbeat pulse.
Have a look at the full gallery here: https://imgur.com/a/1HTYAHN
These creatures are all based around an open source Creature Control Board I designed (https://imgur.com/a/RHuH2bU and https://github.com/harryiliffe/creature-control-board)
I would love some feedback and to answer any questions.
