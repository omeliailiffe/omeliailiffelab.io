---
onepage-id: "fp"
title: "Design Criteria"
description: "Version 3.0"
nav-color: "sem1"
order: 31
---
#### Design Criteria
Designs will involve post-human theory and as such creatures are to be mindful, with wants, dreams and ambitions that fall outside of the human realm.

##### Physical Requirements
As laid out by Ribeiro and Paiva, any organism created will need to be compliant with the 12 principles of animation (where applicable to robotics). This means considering how a robot moves, its behaviour, and its overall appeal.
The creatures designed must sit within the world naturally, and not look out of place, sharing a design language that bonds them together as a group, and blends them in with their environment.
Creatures will be developed using open source principles and each of them documented thoroughly to allow other to recreate them. As such, the technology utilised, and processes used to create, should be open source and accessible.
The creatures should be made out of a range of high quality materials, leading to a diverse group, allowing for different material experiences. Some warm and soft, others hard and spiky.
They should each be robust and durable, able to be handled roughly for extended periods of time.


##### Behaviour
Each creature will have an integrated emotional model and a personality type. Personality types will differ between each creature and will create unique interaction experiences.
The creatures should have a level of communication between each other allowing for a herd mood and dynamic.
Creatures will be able to detect and respond to users in such a way that creates a unique interaction.
