---
onepage-id: "fp"
title: "Final Design Presentation"
nav-color: "sem2"
order: 51
---

## Overview
* intro
* future
* aim
* techniques
* creatures
  * embryo
  * adolescent
  * adult
* circuit board
* behavioural
* Exhibition
  * Measuring Success
*
* Open source
* close

### Introduction

Kia Ora everyone,
I have designed a new species that brings the future of non-organic creatures to today.
My project is titled Post-human Life: Open source non-organic creatures.
### Future
In our current world, robots are created with human centred purposes, designed to for-fill simple tasks delegated to them by humans.
As technology, and the accessibility of technology develops, a new domain of life will be created.
The creatures in this domain (previously thought of as robots) have evolved beyond serving humans and are no longer human centred. They exist in their own ecological niches, seeking not to achieve the goals of humans but their own wants, dreams, and ambitions.

Working in this future domain of life, I have **developed** a new species, unique from current lifeforms on earth. As they exist *outside of the human realm*, calling them robots no longer seems fitting so I am referring to them as non-organic creatures. Different from the organic animals we find today, but *different from their robotic human serving counterparts*.

These non-organic creatures will be situated in an experiential, educational, exhibition for adults and children alike.

### Aim
The exhibition and the creatures are designed to raise questions about life, robotics and non-organic creatures. It will introduce people to non-organic creatures, challenging their perceptions and hopefully making a few uncomfortable about the future.

<!-- ### Techniques
Before I present the creature I have designed, I wanted to briefly touch on some of the techniques that make these creatures possible. These techniques cover two fields, some more about the development of the creature and others focused on the physical and behavioural aspects.
###### Developing Techniques
The techniques of purpose, emotion, and story have helped develop the creature, taking it from an idea to something that people see as believable.
###### Physical and Behavioural techniques
Physical techniques, techniques such as easing in and out of motion, using a range of different expression methods, like colour, sound, and motions, and the presence of secondary actions such as blinking, small movements, and a heartbeat. *These assist the creature communicating with humans, and create situations for humans to apply empathy.* -->

### Creatures
I have designed a species that inhabits the deep areas of the ocean, and have produced 3 creatures each at different stages of life. An Embryo, an adolescent and an adult.

#### Embryo
The Embryo marks the beginning of new life for this non-organic creature and is planted by an adult creature in shallower waters.
The delicate developing bone structure is concealed within a soft but protective membrane. The membrane is tethered to a rocky outcrop and gathers organic particles from the surrounding waters.
The Embryo begins forming strands of yarn that will eventually become part of its thick outer skin. This yarn is currently soft and weak but over time it will strengthen. The first fully yarn structures to appear are the creatures large ear like appendages. These are vital for the creatures survival outside of its egg membrane as it uses them to navigate the ocean currents.
Whilst the embryo remains tethered it is vulnerable to predators interesting in eating its nutrient rich yolk. As such it develops colouring changing pigments to camouflage itself again the rocky or sandy surface.
To further help defend against predators, embryo are often clustered together.
When in immediate danger an embryo will communicates with other embryos developing in the area and gives off a bright beacon of light (bioluminescence) to scare predators off. Other embryo in the area respond and create a waving, pulsing effect.

As the embryo develops the natural growth of the skull, bones, tentacles and skin  break through the membrane and sever the tether. The creature is now considered an adolescent.

#### Adolescence
In this stage the creature begins swimming down into the depths of the ocean, gathering materials from floating organic matter, and small sea creatures.
Those two ear like fins are now strong muscles used to propel the creature through the water.
It also has 8 stubby tentacles it uses to direct material towards its mouth concealed underneath its body.
This adolescent has sensitive nerve endings covering its body. It uses these to better understand its surroundings, detecting the movement of the currents and any particles it bumps into. It relies heavily on these as its eyes are often not fully developed.

Similar to the embryo, the adolescent uses colour changing pigments and bioluminescence to camouflage and deter predators.

The adolescent uses the materials it gathers to grow into the species final form, an adult.
#### Adult
The adult creature resembles the adolescent in many ways although is considerable larger. The permeable membrane from the embryo has been completely replaced by the thick woven yarn that covers the creatures upper body. This skin is durable and strong. Webbing has developed in between each of tentacles, allowing for efficient swimming however the ear-like appendages are still needed to finer control.

Whilst still employing its skin pigments for camouflage and defence, it now does so in select areas of cartilage on its back.
It also using its bioluminescence for a completely different function, **attracting a mate**. Intricate dances are preformed to empress another one of its kind. If the other adult is impressed by the display of talent, it preforms its own dance. If the impression is mutual the two creatures dance together. This process resembles many organic creatures, however the process of mating is quite different. Instead of sharing information in the form on DNA embedded in a gamete, like all non-asexual life, they simple share information via radio-waves. This sharing of information includes details about how each creature lives its life. How it uses its resources, how it develops embryos, areas of the environment to avoid, and areas which were food is abundant. This information is consolidated and used by both creatures to improve the quality of their lives and increase the survival chances of their young.

Once the creature has stored away enough material and food, it travels upwards to plant embryos in the shallower waters

### Technology
These creatures, whilst representing a future non-organic creature that could exist, exist as an illusion. To power this illusion, I've designed a range of technologies.

The first is the Creature Control Board.
The printed circuit board can be thought of as the brain of each creature.
It includes
* a processor
* capacitive touch abilities
* a speaker
* and a gyroscope

These things are all implemented on a single board that I have designed and fabricated.

In addition to this, I have designed a number of mechanisms that allow for unique movements in each creature, a flexible circuit that gives the embryo colour and a heartbeat.


### Exhibition
The reason I began exploring non-organic creatures is because I am fascinated by the reaction and response they cause in humans.

As such I'm developing an experienced based installation, where people are invited to come, experience the illusion of life in the species I have designed and provoke conversations about life, robotics, and the growing presence of non-organic creatures.

The exhibition is designed to be sit within a shipping container and travel to reach a range of different communities such as schools, libraries, museums, art exhibits and festivals.

The walk through style exhibition is divided into two different areas.

The first is an informational section that aims to contextualise the creatures participants are about to interact with.
This will consist of information on the techniques used to design the creatures, artefacts such as bones, fossils and diagrams and examples of other non-organic creatures that exist or may exist in the future.
There is also an interactive element that allows for the brainstorming and creation of non-organic creatures.

Participants will then flow through into the second half of the container. This half is designed to mimic the habit of species I have designed. Projections of water reflections light the room in an eerie blue, and rocky outcrops protrude from the wall.
Participants encounter a large cluster of embryo suspending from the ceil and covering the rocky wall. If the embryos are approached by a hand they panic, sensing danger, and are sent into a colourful frenzy. Moving further along the participants are inviting to observe the creatures inhabiting the rocky outcrop. These creatures can be seen interacting with one another, dancing, and sharing details on each others life.
Participants are not discouraged to reach out and interact with the creatures.
Although they are often shy these they will snuggle into a warm person if given the opportunity.

<!-- #### Measuring success
To measure the success of the exhibition I've begun to think about some questions to ask at the
* Did they create their own non-organic creature?
* Did they interact with my creatures?
* Did they exhibition get them thinking about life and robotics?

Data indicating the success will be primarily measured by facilitators at the beginning and end of the exhibition. The facilitator is the last point of interaction after participants have walked through the exhibition. They gauge the experience of the participants, Do they look happy, are they deep in thought etc. In addition to this, logging of the participants interactions throughout the exhibition can assist in measuring success, ie - did they hold the creatures, did they linger around the embryo. -->

#### Open Source
An underpinning theme to my project has been making the technology easily accessible. This theme has impacted on the materials and processes I have used. By releasing documentation of my own process, the technologies I've developed, and the patterns I've designed I hope to contribute to the development of more of these non-organic creatures.
