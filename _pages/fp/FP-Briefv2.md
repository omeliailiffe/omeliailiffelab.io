---
onepage-id: "fp"
title: "Design Criteria"
description: "Version 2.0"
nav-color: "sem1"
order: 31
hidden: true
---
#### Design Criteria

My final designs will be created to invoke a conversation about life, non-organic creatures and their place in our world. As such it will need to fulfil a number of criteria.

##### Physical Requirements:
As laid out by Ribeiro and Paiva, any organism I create will need to be compliant with the **12 principles of animation** (where applicable to robotics). This means considering how a robot moves, it's behaviour, and it's overall appeal.

The creatures I design must sit within the world naturally, and not look out of place. However, they must also **share a design language** that bonds them together as a group.

The creatures will be developed using **open source principles** and each of them documented thoroughly to allow other to recreate them. As such, the technology utilised, and processes used to create, should be **open source and accessible.**

The robots should be made out of a **range of materials**, leading to a diverse group, allow for different material experiences. Some warm and soft, others hard and spiky.
The should each be robust and durable.

##### Behavioural Requirements:
The main goal of this project is to create non-organic life and the behavioural elements will make or break the illusion.
Each creature will have an **integrated emotional model** and **personality type**. Personality types will differ between each robot and will create different interaction experiences.
The creatures should have a level of **communication** between each other allowing for a herd mood.
The creatures will be able to **detect and respond to users** in such a way that creates a unique interaction.










Primary User:

Secondary User:
