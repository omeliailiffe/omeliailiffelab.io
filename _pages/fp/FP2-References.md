---
onepage-id: "fp"
title: "References"
nav-color: "primary"
order: 98
---
#### Moodboard references

Sea inspiration
Material/Construction inspiration
Museum inspiration

1. https://www.youtube.com/watch?v=bc3YvOprd9M
2. http://mentalfloss.com/article/70944/eerie-beautiful-photos-deep-sea-creatures-marine-biologist - Alexander Semenov
3. http://mentalfloss.com/article/70944/eerie-beautiful-photos-deep-sea-creatures-marine-biologist - Alexander Semenov
4. https://www.thisiscolossal.com/2016/05/new-felted-toy-specimens-by-hine-mizushima/ - Hine Mizushima
5. https://www.thisiscolossal.com/2016/05/new-felted-toy-specimens-by-hine-mizushima/ - Hine Mizushima
6. https://www.thisiscolossal.com/2016/05/new-felted-toy-specimens-by-hine-mizushima/ - Hine Mizushima
7. https://oceanexplorer.noaa.gov/okeanos/explorations/ex1402/logs/apr28/apr28.html
8. https://caitlintmccormack.com/artwork/4410762-Wolfbiter.html - Caitlin Mccormack
9. https://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=12187738
10. https://reefs.com/2018/04/14/the-severe-health-risk-posed-by-the-caribbean-condylactis-anemone/
11. https://oceanexplorer.noaa.gov/image-gallery/welcome.html#cbpi=/okeanos/explorations/ex1806/logs/june26/media/bobtail-2.html
12. Te Papa Self-Photographed
13. Te Papa Self-Photographed
14. Te Papa Self-Photographed
15. Te Papa Self-Photographed
16. Te Papa Self-Photographed
17. http://quickdraw.withgoogle.com
18. Muséum national d’histoire naturelle
19. https://www.the-scientist.com/image-of-the-day/image-of-the-day-dumbo-octopus-hatchling-30259



https://www.invisionapp.com/inside-design/museum-exhibit-design/

#### Academic References
<div class="embed-responsive embed-responsive-4by3">
<iframe class="embed-responsive-item" src="https://api.zotero.org/users/5590667/collections/TUBBUPIE/items?format=bib&style=mla&key=7bLzAqimRdnxP61J6UNVprTP"> </iframe>
</div>
