---
onepage-id: "fp"
title: "S1 Presentation"
nav-color: "sem1"
order: 38
---

Things to include in final presentation
* Starting point
* Secondary Research
* Primary
* Concepts


### Starting point
My project focuses on researching techniques and technologies that blur the line between the organic and non-organic, the real and the almost real. (Photos on screen, Catapiller vs Furworm, something vs something)

### Areas of secondary research
I delved into secondary research, looking at existing projects such as noodlefeet and kismet, exploring techniques such as the need illusion and research how to implement complicated emotional models which take how we know emotions to work in human brains and emulate them on a micro-controller.

The creatures that I have designed, I am calling non-organic creatures.

### Primary
For my primary research I gathered together a focus group of creative people, and asked them to dream up their own non-organic creatures using the techniques discovered through my primary research. They were asked to exploring the creatures role in their environment, and figure out how they will react to different situations. From this I was able to see a rich range of different creatures, all with different wants, needs and desires.
༼ つ ◕_◕ ༽つ give example༼ つ ◕_◕ ༽つ
One of the creatures created loves fashion and would try on anything it found to find the perfect cape, and another who desired to spread information and would either jump up in your face and yell at you or print and stick flyers everywhere.

Through the focus group, and the creatures they created, I gained an understanding of how people see non-organic creatures existing, the spaces the might inhabit and how they might interact with people.

Techniques
So let me walk you through some of the key techniques to creating your own non-organic creature, whilst showing you one of my own creations.

This is Fox. Fox is a cute cuddly creature who loves warm hugs.
The first technique is purpose. Purpose defines what your creatures goals are in life. Fox's purpose, giving and receiving warm hugs is what she does best.

The second technique is emotion, adding emotions even simple emotions to a creature can make it feel much more alive.
Fox is often shy and will sometimes freeze up with fear, but if she is comfortable and happy she will hug you back. This is how Fox expresses her emotions.

The final technique is story. Story fills in all the little knowledge gaps we have with a creature. It can be thought of as sort of like a backstory.
So Fox, she was born on the Himalayas but unfortunately, due to her small size,  she was abandoned at birth. After being rescued she was brought to New Zealand to try to be re-introduced to a flock of sheep.
She is left craving the warmth of her flock.


The reason I began exploring non-organic creatures, like fox, is because I am fascinated by the reaction and response they cause in humans.

As such my final concept is an experienced based installation, where people are invited to come, experience the illusion of life in the creatures I create and provoke conversations about life, robotics, and the growing presence of non-organic creatures.
