---
onepage-id: "fp"
title: "S2 Initial Presentation"
nav-color: "sem2"
order: 50
---

### Introduction

Kia Ora everyone,
My name is Omelia Iliffe and I'm looking at Post Human Life: Open source Non-organic creatures.

### Starting point
My project focuses on researching techniques and technologies that blur the line between the organic and non-organic. (Photos on screen, Caterpillar vs Furworm )

### Post-human
My project is has a post-human foundation. Rosi Braidotti says "Post-human is a generative tool to help us re-think the basic unit of reference for the human." Post-human theory is an important foundation to my project as it can be used to create creature that aren't humans focused.

That course marked the beginning of my exploration into this future space of how these entities can have great influence on our human emotions, wether we want them to or not.

### Aim
Through this project, I want to raise questions about life, robotics and non-organic creatures, challenging people's perceptions and making hopefully, make a few people uncomfortable along the way.

### OPSORO
Throughout my research I discovered a number of projects working in similar avenues. I'll talk about 2 that I have evaluated, OPSORO and NoodleFeet.
OPSORO is an open source hardware platform that allow people to build their own expressive robots more easily.
This project approaches the task of creating creatures from a purely technical view. It lacks an understanding of why these creatures were created and how they should fit into our world, preventing the creations from becoming anything more than fancy robots that serve humans.


### NoodleFeet
One person who is really delving into this other-side of development is Sarah Petkus, with her creation, Noodlefeet.
Noodlefeet is a young robot, conceived by Sarah in 2015. Over time  Sarah found she was increasingly referring to noodle as her "offspring" or "child" and referring to herself as mother. This way of thinking (mother and child) became critical to the way noodlefeet developed. Noodle transitioned from being a robot into being a creature with wants, ambitions and dreams. As noodle acquires new hardware and software updates, these developments thought of as "growing". Sarah, just as a mother would, wants to see noodle succeed and is doing everything she can to help realise his dream.

### Achieving life
So how do we successfully create a creature that, when framed through the post-human theory, takes on a life of its own.
A key researcher working in this space is Christine Sunu. Sunu designs complex, effective, and successful experiences. Through creating creatures, she has developed a series of requirements that can draw people in, bring a creature to life, and allow for the formation of an empathetic connection.  
They are purpose, emotion and story.  
Using these requirements as a foundation, I developed a framework that allows for the creation of non-organic creatures.


Using these techniques as a foundation, I developed a framework that allows for the creation of non-organic creatures.

### The Framework
So let me walk you through this framework I created and show you one of my own early creations.

This is Fox. Fox is a cute cuddly creature who loves warm hugs.
The first technique is purpose. Purpose defines what your creatures goals are in life. Fox's purpose, giving and receiving warm hugs is what she does best.

The second technique is emotion, adding emotions even simple emotions to a creature can make it feel much more alive.
Fox is often shy and will sometimes freeze up with fear, but if she is comfortable and happy she will hug you back. This is how Fox expresses her emotions.

The final technique is story. Story fills in all the little knowledge gaps we have with a creature. It can be thought of as sort of like a backstory.
So Fox, she was abandoned at birth, due to her small size and is left craving the warmth of her flock. This is why she gives such good hugs.

### Primary
To validate this framework I gathered together a focus group, and asked them to dream up their own non-organic creatures.
<!-- They were asked to work through the framework, exploring the creatures role in their environment, and figure out how they will react to different situations.  -->
From this I was able to see a rich range of different creatures, all with different wants, needs and desires.

One of the creatures created loves fashion and would try on anything it found to find the perfect cape, and another who desired to spread information and would either jump up in your face and yell at you or print and stick flyers everywhere.
Each creature communicated its emotions differently ranging from changing skin colours to drooling.

Through the focus group, and the creatures they created, I gained an understanding of how people see non-organic creatures existing, the spaces the might inhabit and how they might interact with people.


### Exhibition
The reason I began exploring non-organic creatures, like fox, is because I am fascinated by the reaction and response they cause in humans.

As such the concept I'm developing is an experienced based installation, where people are invited to come, experience the illusion of life in the creatures I create and provoke conversations about life, robotics, and the growing presence of non-organic creatures.

The Exhibition will consist of 3 main parts.

#### Menagerie
A Menagerie is a collection of wild animals kept in captivity for exhibition.
The main focus of the Exhibition will be a family of creatures.
These creatures will act as we would expect organic animals to act, interacting with each other and the onlookers.
As inspiration for the forms of these creatures, I have looked to the ocean.
Ocean and deep sea creatures are unfamiliar, almost alien like.
I want to incorporate these feelings of other/alien into the creatures whilst still having them inspired and derived from something of our own earth.

I am currently experimenting with the form and materiality of these creatures. I've looked a sewing patterns, and crochet, and will also be exploring resins.

#### Artefacts
Secondly, I will be designing a range of artefacts related to the creatures. These section will be reminiscent of a museum exhibit, with bones, fossils, diagram, etc. It will help to communicate the story of the creatures living in the wild, and contextualise the Menagerie.

#### Crowd Sourcing (Accessibility)

Finally, as a large portion of my research was compiling a framework that allowed people to think about and create their own non-organic creatures, a natural progression is to make the tools more accessible. I am looking at ways to distribute the framework, and collect crowd-sourced data of non-organic creatures.

I'm putting together a website, that allowed people to dream up their own creature and send it back to me. These creatures will then be displayed as part of the exhibition.

### Next Steps

From here, I am resolving the final form of my creatures, designing the interactions between creatures and people, developing a range of artefacts, and distributing the framework to receive crowdsourced creatures.
