---
onepage-id: "fp"
title: "Exposure"
nav-color: "sem2"
order: 90
---

### Short Summary <small>(~70 words)</small>

My project focuses on implementing techniques and technologies that blur the line between organic life and robotic or "non-organic" life.
Working in this future space, I am developing a species of non-organic creatures.
Through these creatures, I am raising questions about life, robotics and non-organic creatures, challenging people's perceptions and provoking conversations about the growing presence of non-organic creatures.


### Short Summary <small>(~70 words)</small>

My project focuses on implementing techniques and technologies that blur the line between organic life and robotic or "non-organic" life.
Working in a future space, I have designed a species of non-organic creatures.
Through these creatures, I am raising questions about life, robotics and their intersections, challenging people's perceptions and provoking conversations about the growing presence of non-organic creatures.


### Final Exposure Submission

Omelia Iliffe

Post Human Life: Open Source Non-Organic Creatures

“Working in a future space, I have designed a species of non-organic creatures, focusing on implementing techniques and technologies that blur the line between organic life and robotic or "non-organic" life. Through these creatures, I am raising questions about life, robotics, and their intersections, challenging people's perceptions and provoking conversations about the growing presence of non-organic creatures.”

Acrylic Yarn, Crocheted,
Sheet Polypropylene

Industrial Design

omelia@iliffe.io

omelia.iliffe.io

www.linkedin.com/in/omelia

Captions

01:
The Adolescent creature,

02:
The family of highly interactive creatures.

03:
The Embryo, the youngest in the family, encased in a squishy, slimly membrane.

04:
Designed from the ground up with interaction in mind, the embryo is formed to fit snuggly into cupped hands.
