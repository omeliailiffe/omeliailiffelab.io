---
onepage-id: "fp"
title: "Context Review"
nav-color: "sem1"
order: 22
---
The aim of the context review is to learn enough about the topic to develop
depth of knowledge as well as identify gaps and opportunities. A context
review touches on micro and macro issues across people, culture, technology
and society. This could include (but is not limited to):
- State of the art: competitive situation, review of current products and
technologies
- The market: who the users are, context of use, description of design issues,
latent needs;
- Trends: emerging technologies and materials, changes in markets, culture
and society

#### Current Products/Projects

<!-- ##### WAMOEBA
Developed in 2011 -->

##### OPSORO
"OPSORO stands for Open Platform For Social Robotics and is more than just a robot. OPSORO democratizes social robot technologies and is meant for everyone. Not only techies, makers or developers, really everyone! Age or gender doesn’t matter." <small> [www.opsoro.com](http://www.opsoro.com/)</small>

OPSORO has been developed over a number of years, starting with a robot, ONO, and moving to a system that allows everyone to create their own social robot.

Although each robot is built on the same open source technology, their system creates an easy way for the user to create their own social robot skin. This means each robot can look and function unique.

<div class="row">
  <div class="col-6">
    <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Meet Walu, the Octopus Class Robot made by Claire Vandenameele and Yannick Stoelen. 🐙 He can change colors and kids adore him! <a href="https://twitter.com/hashtag/skins?src=hash&amp;ref_src=twsrc%5Etfw">#skins</a> <a href="https://twitter.com/hashtag/robot?src=hash&amp;ref_src=twsrc%5Etfw">#robot</a> <a href="https://t.co/mtpjAUIcVr">pic.twitter.com/mtpjAUIcVr</a></p>&mdash; opsoro (@opsoro) <a href="https://twitter.com/opsoro/status/883596648697536512?ref_src=twsrc%5Etfw">July 8, 2017</a></blockquote>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>
  <div class="col-6">
    <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Everyone: meet Gérard. He&#39;s a mix between a giraf and a robot and the most loyal friend a person could wish for! <a href="https://twitter.com/hashtag/skins?src=hash&amp;ref_src=twsrc%5Etfw">#skins</a> <a href="https://twitter.com/hashtag/social?src=hash&amp;ref_src=twsrc%5Etfw">#social</a> <a href="https://twitter.com/hashtag/robotics?src=hash&amp;ref_src=twsrc%5Etfw">#robotics</a> <a href="https://t.co/IRCOKlCGSw">pic.twitter.com/IRCOKlCGSw</a></p>&mdash; opsoro (@opsoro) <a href="https://twitter.com/opsoro/status/892701722778513409?ref_src=twsrc%5Etfw">August 2, 2017</a></blockquote>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>
</div>

##### Poppy-Project

The Poppy Project has created a number of different open source robots. Poppy Humanoid, a humanoid robot; Poppy Torso, similar to Poppy Humanoid but without legs; and Ergo Jr, a robot not dissimilar to the Pixar Lamp.
Their shared vision is that "robots are powerful tools to learn and be creative."
{% include image.html id="poppy" images="/assets/img/fp/poppy.jpg" %}
<small>[poppy-project.org](https://www.poppy-project.org/en/)</small>
##### Romibo

Designed to be a low-cost prototype kit to allow more social therapy research to be completed, Romibo is a small robot, who can move around and hold eye contact with children.
{% include image.html id="romibo" images="/assets/img/fp/romibo.jpg" %}

<small>[Webarchive](https://web.archive.org/web/20130531014635/http://www.romibo.org/therapy/)</small>

##### NoodleFeet

Sarah Petkus is an artist, who began creating Noodle in 2015. Over time she began referring to Noodle as her 'child' or 'offspring' and referring to herself as 'mother'. As Noodle acquires new hardware and software updates, these changes are thought of as 'growing'.

Petkus concludes her artist statement saying "I think the way we relate to our technology must shift and evolve as we continue to innovate. Whether a creation serves us in a utilitarian way, or acts as a unique extension of the individual who created it, we should continuously challenge where we draw hard lines between the two, and where what we create overlaps with our sense of self."
{% include image.html id="noodlefeet3" images="/assets/img/fp/noodlefeet3.png" %}
<small>[noodlefeet.zoness.com/](http://noodlefeet.zoness.com/)</small>
