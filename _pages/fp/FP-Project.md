---
onepage-id: "fp"
title: "Project Proposal"
nav-color: "sem1"
order: 21
---

### Post-human Life: Open Source Emotive robots

#### Project Aim
I want to explore technology that pushes and blurs the line between robotics and organic life to design open source emotive robots.


#### Topic Description

I want to explore how to use cheap and accessible technology to build robotics that create the illusion of life through a number of different techniques.
A key researcher is Christine Sunu. Sunu, in her TEDx talk, presented the idea of "the Need Illusion". She highlighted a few key concepts that, when implemented well, can create a robot that not only feigns life, but demands empathy from its users.
My aim is to explore these concepts, biomimicry and technology to blur robotic design with organic life, and make it more accessible to a wide range of everyday people.


#### Keywords
 * Open Source
 * Robotics
 * Biomimicry
 * Accessible Technology

#### Why did you choose this Topic?
In 2018, I wrote an essay (for Creative Culture and Ideas: Studies on post-human) discussing the presence of the artificial non-human entities that are becoming increasingly present in our day to day lives. I had also just designed a prototype wearable pet and both of these captured my interest. I haven't been able to stop thinking about it since.
#### What interests you about the topic?
From circuits and programming to interaction design, this project has a wide range of opportunity for me to show my skills. I have been interested in the idea of living robotics and want to create an eye catching project.
#### How this topic will help your design career?
Being successful in the open source world, means being visible and having a following. I would like to tackle a large project such as this to launch my open source presence online and around the fab network.

#### Research Questions

1. What technology is available for open source emotive robots?
2. What qualities contribute to a robot being perceived as alive?
3. How are emotions most effectively communicated?
4. How might emotive robots improve users quality of life?
5. What emotive products exist currently, and how do people relate to them?

#### Secondary Research

Some interesting people:
Christine Sunu (Robot Designer)
Kismet and Cog (A research project from MIT in the late 90s/early 2000s)
Sarah Petkus (Mother of her robot spawn, Noodle Feet)

I will also be researching Biomimicry, kinematics and qualities to make robots feign life.

#### Primary Research

I hope to engage in generative design

#### Mood-board
{% include image.html id="moodboard" images="/assets/img/fp/moodboard.png" %}
#### Trend Map

Reliant on users - Independent

Serving - Self-serving

Robotic - Life-like
{% include image.html id="trendmap" images="/assets/img/fp/trendmap.png" %}



#### Inital Concepts

* Collaborative Wifi hunters
* Emotive pet robots
