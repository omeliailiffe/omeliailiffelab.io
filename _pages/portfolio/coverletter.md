---
category: pages
layout: default
title: "Cover Letter"
description: "Omelia Iliffe"
permalink: /coverletter/
contact_nav: true
nav_links: false
---

I am very excited about the opportunity to work in a creative, well-equipped and innovative space.
I believe I would be an excellent addition to the team and
look forward to discussing this with you.

For the past 5 years, I have been involved with Fab Lab Wgtn, a makerspace at Massey
University.  
The excitement I had when I first discovered Fab Lab Wgtn, and the idea and ideals of a
makerspace, actually changed my direction at University. I loved the open space to explore
ideas, the ability to play, and the value placed on life-long learning. I’ve had the incredible opportunity to participate in Fab Lab Wgtn’s community for many years - making the space accessible, developing courses and materials, and enjoying helping others explore and play.

I’ve also had the opportunity to engage with Fab Lab Wgtn’s wider community through
popup labs. For the past 3 years, I have been part of the Fab Lab Wgtn team hosting the
popup lab at MakerFaire Wellington, bringing our values of play and exploration to hundreds of
people of all ages. I have visited high schools to exhibit digital technologies in an interactive
and engaging way for students and staff. And in 2018, I hosted a pop-up lab at Pataka
Education Space, where we explored themes of Matariki.

Throughout my time at Massey University, I have become skilled in a wide range of fabrication
processes, often going beyond the prescribed teaching to truly understand a process.
These include 3D printing, laser cutting, sewing, electronics, and CNC milling. When faced
with a new process or machine, I’m always willing to discover its inner workings and how
to best use it.

I understand the importance of flexibility when working with modern technology, as it is
always evolving. As software and processes have become obsolete or outdated at Fab Lab Wgtn,
I have found new solutions, teaching myself and then passing on that knowledge. This
happens regularly as new processes and software are constantly being developed, and I
keep a keen eye out for developments. This constant learning process also keeps me
grounded, and helps me to have empathy for other learners. When teaching, I enable people to
learn at their own pace, meeting them where they are at and supporting them in their
learning journey.

I believe these examples I have given demonstrate a great ability to carry out the role of
Technical Demonstrator. I look forward to hearing from you!
