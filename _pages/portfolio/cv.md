---
category: pages
layout: cv
title: "Curriculum vitae"
description: "Omelia Iliffe"
permalink: /cv/
contact_nav: true
nav_links: true
nav_current: cv
collapse_work: true
---
### About Me
I am a passionate, hardworking individual, who loves thinking creatively, exploring technology and empowering others through teaching.
