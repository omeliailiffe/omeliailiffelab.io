---
category: pages
layout: cv
title: "Curriculum vitae"
description: "Omelia Iliffe"
permalink: /cv/print
contact_nav: true
nav_links: false
collapse_work: false
---
### About Me
I am a passionate, hardworking individual, who loves thinking creatively, exploring technology and empowering others through teaching.
