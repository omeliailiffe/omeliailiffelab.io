---
layout: olddefault
category: pages
title: "Internship"
permalink: /internship-a2/
description: "Assessment 2"
navbar: [About Fab Lab WGTN, Internship Project, Goals, Confidentiality Agreement]
---


{:#about-fab-lab-wgtn}
# About Fab Lab WGTN

### Company Structure

{% include image.html id="sheet2" images="/assets/img/internship/structure.png" %}

#### Fab Antipodes

From [fabantipodes.org](fabantipodes.org): *"Fab Antipodes was initiated by delegates from Fab Lab Wgtn and Fab Lab WA, in 2014 at the 10th global Fab conference in Barcelona. We are a growing group of fab labs who provide local knowledge, experience and support to each other and to groups who are starting new labs in the region.We work on projects across fab labs in the region and around the world, connecting and enhancing our individual resources and skills."*

### Sustainability Practices: The Resilience Project

*"We believe that resilience is essential in these exponentially changing times, and that it is built through hands-on learning in global open design ecosystems.
We are striving towards adaptability through economic, environmental and technological changes. We are inquisitive and love exploring ways to become more open, self-sustaining and circular in our workflows and processes."*


At Fab Lab WGTN, the Resilience Project is incorporated into each decision they make from materials to buy and use, projects they take on, and how they deal with waste.

A large number of the materials used at Fab Lab WGTN, such as plywood and bamboo ply, are purchased from verified renewable sources. Other materials that contain harmful chemicals (such as MDF and ABS plastic) are banned from being used.

Another big part of the Resilience project is what happens to waste coming out of the lab. Unfortunately, as people are making and producing things, waste is produced. Fab Lab WGTN deals with this in a number of ways.
Compostable waste is put in worm farms that are maintained every few months. As much recycling is done as possible with the university providing bins. Smaller things, such as broken circuit boards and material scraps, are kept for soldering practice and tests.

Wendy and Craig were also experimenting with burying waste poplar plywood in their garden to return the nutrients to the ground.

### Design Influences and Processes

A Fab Lab is a platform for learning and innovation: a place to play, to create, to learn, to mentor, to invent. A Fab Lab is a digital prototyping space providing stimulus for local creativity.

To be involved with a Fab Lab means connecting to a global community of learners, educators, technologists, researchers, designers, makers and innovators - a knowledge sharing network that spans 80+ countries. As all Fab Labs share common tools and processes, the global network is a distributed laboratory for research and invention.

There are a number of key things that influence the projects that happen at Fab Lab WGTN. <br>
One of these, as mentioned above, is **The Resilience Project**.<br>
Another is the **Connectedness** of the project. Fab Lab WGTN is part of a *"global community of learners, educators, technologists, researchers, designers, makers and innovators"* connected through other Fab Labs across the world. Fab Lab WGTN take on projects that draw from this global network of skills and allow collaboration across the community.<br>
A final key influence is **Openness**. Fab Labs are built on a base of open source processes and technology and, as such, will only take on projects that have an open element to them. To be open source is to document the process and release the files, allowing anyone else to recreate and further build on what Fab Lab WGTN have done.

A great project that summarises these 3 elements is the Open Source Bee Hive project. This project was developed in Barcelona but due to it being open source, Fab Lab WGTN were able to participate and contribute to the project.

<iframe class="w-100" src="https://player.vimeo.com/video/87478670" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/87478670">Open Source Beehive Made in New Zealand!</a> from <a href="https://vimeo.com/opensourcebeehives">Open Source Beehives</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


### What Fab Lab WGTN make
Although Fab Lab WGTN does take on projects, they mainly run, organise or participate in events.
#### Intro Sessions
Being connected to Massey University, Fab Lab WGTN spend a lot of time serving students that wish to use the processes available. Workshops are introductory sessions that give people the base skills needed to use a machine or process.
These are run weekly and by demand.

#### Workshops
Fab Lab WGTN also offer more in depth workshops, were people often new to the lab can come in, learn an entire process, and take home something interesting.
These workshop are normally run for organisations or for specials occasions.
#### Pop Up Labs
Pop Up Labs are all about getting out of the Lab and bringing a few processes to a different group of people.
A few times this year, Fab Lab WGTN have run a Pop Up Lab around the university, in the Tea Gardens, Block 12, and Block 1.
During my internship, we ran a Pop Up Lab at MakerFaire Wellington.

#### Services
Fab Lab WGTN also offer basic services. They tend not to take on purely manufacturing projects, but if a project offers something back to the lab or is an awesome project it will be considered.
Services might include, cutting and finishing wood parts, designing a circuit board, 3d modelling a product.

#### Target Market
Fab Lab WGTNs target market is often limited by the need to serve the students of Massey. A lot of the year is spent running Intro Sessions and helping them with projects that there often isn't a lot of time left for others. This is something Wendy and Craig have identified, and would like to change by running more workshops for people outside of the University.


### Recruitment
As Fab Lab WGTN is small, with just Wendy and Craig as the core staff, and they have a wide network of friends of the lab who are available to help if more staff are needed, recruitment doesn't happen all to often.
However when it does, it is often through the global network of Fab Labs.
When Wendy left to take part in Fab Lab Dubai, Etienne was found through a Fab Lab in France. Other people, like myself, get involved just through being in the space lots and building a good relationship with Wendy and Craig.

{:#internship-project}
# Internship Project
At the beginning of my internship, I was just doing half a day a week, helping out with the generally running of the lab. I was given the task of updating the software used by the Digital Embroidery machine, and as such, I was a natural choice to teach other students the process. I ran 3 Digital Embroidery sessions over my internship, with about 6 students each time.
I spent 20 hours doing this.

My main project for the internship was to reawaken and work on an old project of mine, the [Interactive Wall](http://omelia.iliffe.io/interactive-design). I began working on this project with Fab Lab WGTN during semester 2, 2017, and Wendy wanted me to keep developing it. The progress can be seen on the [Interactive Wall Project Blog](http://omelia.iliffe.io/interactive-wall).
Over the past 4 weeks I have spent 55 hours working on this.

At the end of my internship I presented the interactive wall to Wendy and Craig, who were pleased with my progress. I have been invited back next summer to complete the remaining work left on this project.

{:#goals}
# Goals
Below are the goals I made before my internship and some feedback on how well I think I achieved them.

**Through my internship I want to gain more skills teaching and educated others. By the end of my internship I will be able to talk confidently in front of a group and explain how the company operates.**<br>
I started my internship with working on the Digital Embroidery machine and ended up teaching other students how to use it. I found that my confidence talking to a group came very naturally after I was familiar with what I was discussing. I was able to answer questions on the fly and didn't get flustered when things didn't go quite to plan.
Through out the internship, I have had to give countless updates on the progress of my project, which has really built up my confidence in my design decisions and talking about my work.
I am confident that I would be able to present to any group if given time to prepare.

**Throughout my internship, I will be positive and enthusiastic about the work I am given**<br>
This on came easy as I always really enjoyed the work I was given to do. However, I was able to put my full effort in even when the work didn't engage me as much.

**At the end of my internship, I will have a better understanding of how to manage and complete a project in the time require. Including quote the correct amount of time for a project.**<br>
This one is tricky. Before re-beginning the interactive wall project, I was asked to create a timeline and different check points to mark my progress. I found this really difficult but it was a invaluable experience. Being able to update it throughout the project and see where my estimations were wrong was very helpful.


{:#confidentiality-agreement}
# Confidentiality Agreement

I am just waiting to receive this form back from Wendy, but in talking to her think that she would have no problem with confidentiality.
