---
layout: olddefault
category: pages
title: "Internship"
permalink: /internship/
description: "Assessment 1"
navbar: [CV, Portfolio, Goals, Companies, Letter, Questions]
---

{:#cv}
# CV

I have created my CV.
Please find it at [omelia.iliffe.io/cv](http://omelia.iliffe.io/cv)

{:#portfolio}
# Portfolio

I have also created an online portfolio to display a few of my best projects.
Please find it at [omelia.iliffe.io](http://omelia.iliffe.io)

{:#goals}
# Goals

- Through my internship I want to gain more skills teaching and educated others. By the end of my internship I will be able to talk confidently in front of a group and explain how the company operates.
- Throughout my internship, I will be positive and enthusiastic about the work I am given.
- At the end of my internship, I will have a better understanding of how to manage and complete a project in the time require. Including quote the correct amount of time for a project.


{:#companies}
# Potential Places

##### National Library 3D Printing space

The National Library has a small 3D printing space set up in at their Thorndon campus. Being the national library, the see a wide range of visitors and could be a very interesting place to intern at.

I am interested in work here as it provides great connection to the public (being a public library) and will allow my confidence of talking to new people to grow.

##### FabLab WGTN

Fab Lab WGTN is based in Massey University but is open to the public, offering a open collaborative learning environment. They have a wide range of opportunities for my involvement.

Fablab wgtn is part of a global network of over 1800 FabLabs across the world and, through working collaboratively, FabLabs are responsibly for some of the worlds most prestigious digital fabrication projects.

Fablab wgtn would be a great place for me to intern as it provides a wide range of opportunities that I am passionate about. They also are open to the public and teach people how to use a range of digital fabrication process. Fablab wgtn would not only increase my confidence in teaching but would also provide projects for me to quote times on and complete independently.

##### Mind Lab

Mind Lab is a learning environment with a focus digital and collaborative learning. They offer a range of programmes for all ages and would offer me great experience in teaching.

One of my passions is working with kids, and Mind Labs provides an excellent opportunity for me to get involved through there MindLabsKids program. This would be a merging of my 2 main areas of interest (Design and Children). Mind labs would give me a great foundation and introduction to teaching children.

##### Formway

"Formway Design Studio is a Wellington based design company which has established itself as a global leader in high performance furniture."
Formway are an Industrial design company who design and sell chairs. Working as an intern here, I would gain world class skills and hands-on experience designing furniture at a world famous company.

Although working at Formway would definitely put me outside of my comfort zone, they would provide excellent learning in a real world industrial design studio.


<br>
I have chosen to reach out to **Fab Lab WGTN** for an internship as they offer a wide range of opportunities, have an interesting business model, and are super friendly.

{:#letter}
# Letter

Below is the email I sent to Fab Lab WGTN to propose an internship.

Good Morning, Wendy

My name is Omelia Iliffe, and I am a student at Massey University, College of Creative Arts. I am studying Industrial Design and as part of my course we are required to undertake an 75 hour internship in our field.

I have been involved with Fab Lab WGTN in the past (taken your introductory sessions and used the machines) and have always appreciated the excellent learning environment you create. I would love to get involved more and be a part of something awesome.
As an Industrial Design Student, I have a wide range of skills (designing, using machinery) as well as fantastic people skills.

I think I would make a great addition to your team.
Please check out my online [Portfolio](http://omelia.iliffe.io/portfolio) and [CV](http://omelia.iliffe.io/cv).

I look forward to hearing from you,

Omelia Iliffe

{:#questions}
# Questions

- What are your core values, and what is an example of how this effects your business?
- Who inspires and influences the projects that you choose to undertake?
- What is your long term vision for your business? How you would you like to see your business impacting New Zealand?


I have been contacting Wendy at Fab Lab WGTN.
**wendy@fablabwgtn.co.nz**
