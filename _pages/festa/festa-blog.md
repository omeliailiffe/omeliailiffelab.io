---
layout: olddefault
category: pages
title: "Festa: Blog"
permalink: /festa-blog/
description: "The Blog for all my Creative Industries work"
navbar: [Research, Concepts, Projections, Lighting, Concepts]
navbarbutton: [Reflection, /festa/]

slider0: [/assets/img/festa/concept2.jpg, /assets/img/festa/concept3.jpg, /assets/img/festa/concept4.jpg]
slider1: [/assets/img/festa/testfirst1.jpg, /assets/img/festa/testfirst2.jpg, /assets/img/festa/testfirst3.jpg]
slider2: [/assets/img/festa/testinitial1.jpg, /assets/img/festa/testinitial2.jpg]
slider3: [/assets/img/festa/testfull8.jpg, /assets/img/festa/testfull1.jpg, /assets/img/festa/testfull2.jpg, /assets/img/festa/testfull7.jpg,  /assets/img/festa/testfull9.jpg]
slider4: [/assets/img/festa/testfull3.jpg, /assets/img/festa/testfull4.jpg, /assets/img/festa/testfull5.jpg, /assets/img/festa/testfull6.jpg]
slider45: [/assets/img/festa/onsite5.jpg, /assets/img/festa/onsite1.jpg, /assets/img/festa/onsite2.jpg, /assets/img/festa/onsite3.jpg, /assets/img/festa/onsite4.jpg]
slider5: [/assets/img/festa/onsite6.jpg, /assets/img/festa/onsite7.jpg, /assets/img/festa/onsite8.jpg]
slider6: [/assets/img/festa/action1.jpg, /assets/img/festa/action2.jpg, /assets/img/festa/action3.jpg, /assets/img/festa/action4.jpg, /assets/img/festa/action15.jpg]
slider7: [/assets/img/festa/action5.jpg]
slider8: [/assets/img/festa/action6.jpg, /assets/img/festa/action7.jpg, /assets/img/festa/action8.jpg, /assets/img/festa/action10.jpg, /assets/img/festa/action11.jpg, /assets/img/festa/action12.jpg, /assets/img/festa/action13.jpg, /assets/img/festa/action14.jpg]
---

{:#research}
## Research

Unfortunately as I came into the class from Creative Industries I missed most of the key researching classes.

{:#concepts}
## Early Concepts

#### Week 4/5
In groups we came up with concepts to share to with the class.
Our concept: A sensory maze that would determine which beer you would like.
{% include image.html id="concept5" images="/assets/img/festa/concept5.jpg" %}

To illustrate our concept, I mocked up 2 different tunnels.
The 2 tunnels in my concept illustrated how a sensory maze might work. Participants would choose between the harsh, rapidly changing, light tunnel on the left or the softer pulsing tunnel on the right depending on which felt better to them. This would be one of a series of tunnels/decisions all leading you to the beer most like the choices you made.
{% include imageslider.html id="slider0" images=page.slider0 %}

In groups, we also did some rapid ideation.
{% include image.html id="concept2" images="concept1.jpg" %}
The idea behind our rapid ideation was using pneumatic tubes to move a can of beer and guide a participant through the maze.

{:#projections}
## Team work: Projection, Lighting, and Audio

#### Week 7
To streamline the ideation and design process we spilt into task groups.
I chose Projection. When teams were being established I volunteered for a few different ones but ended up settling on the Projection and Audio team.

Once assembled our team got straight to the testing. We printed off the microscopic test imagery and used an OHP (over-header projector) to test in a dark room. We were testing how bright the images would be and how feasible it was to use the OHP.
The projected images were bright but after 20 minutes of testing the OHP bulb blew leading us to believe the would not be reliable enough.
I also created a quick processing sketch which sent colourful balls moving across the screen as an example of interactive projection. We tested this with a data projector and agreed that, with some more effects and better imagery, it could be much more engaging to have a person interacting.
{% include imageslider.html id="slider1" images=page.slider1 %}

#### Week 8
In our previous, meeting we decided it would be cool to have an interactive projection so I added support for the Microsoft Kinect. This allowed someone to move their hands around and the projection would move with them.
{% include image.html id="interactive" images="/assets/img/festa/testinteraction1.jpg" %}
Whilst this was cool, it was not reliable and wouldn't really work with lots of people around. We decided to look at other ways to facilitate interaction.

We decided that lighting was going to be necessary due to how dark it was going to get in the later hours of the evening.
We contacted Adam from CMP about using some DMX lights and he gave use access to T21. In T21 we were able to test lighting the frost cloth from different angles and with different colours.

{% include imageslider.html id="slider2" images=page.slider2 %}

We regrouped to discussed our narrative after Antony, in front of class, threw hard questions at us:
With the overarching narrative being taking the drink out of the glass and thinking about different ways to experience drinking beer we developed our concept. Each beer is assigned a colour (relating to the flavour/taste), as the consumer is enjoying there beer they are inviting bask in the light of the beer. The lights (positioned on the concrete posts) will change and update depending on which beers are being drunk in each corner of the triangle.

{% include image.html id="plan1" images="/assets/img/festa/plan1.jpg" %}

As much as we liked this narrative and interactivity, we collectively decided interactivity was not were we should be focusing our efforts.

#### Week 9

In our next meeting, we had received a draft structure document so we could begin figuring out the placement of the projectors.

Projector placement became an on-going challenge throughout the project. We originally planned to place a projector atop each of the concrete pillars, however with the final version of the structure document a number of things had been revised making it not practical. We decided that with the frost cloth being dyed and painted, using projected imagery as well would crowd the cloth and make it look too busy.



#### Week 10

With the final structure document confirmed and the materials team settling on the use of marbled frost cloth we decided to set up a full-scale test. We booked the lighting room, hassled CMP for their DMX lights and created a full-scale test to show to the class. This was the first time any of us had gotten a real idea of the scale of this installation.
We set up the lights to be on top of each of the concrete poles (facing the frost cloth). Instead of using a DMX controller to fade the lights between colours, I just set them on AutoShow for convenience but it worked perfectly.
{% include imageslider.html id="slider3" images=page.slider3 %}

When we informed the wider group that using a projector was not practical they were noticeable disappointed and insisted we keep trying to make it work. We set up a projector for the test just to see what it would look like.
{% include imageslider.html id="slider4" images=page.slider4 %}
Most in the class agreed they really liked the projected imagery.
After discussing with the structure team, they highlighted to existing fences on site that we could position a stand with projector attached next to.
{% include image.html id="plan2" images="/assets/img/festa/plan2.jpg"%}
We organised another test to finalise opinions concerning the placement of lights and projectors, and to test the videos that were being created to project.


#### Week 11

The last major test we organised was an outdoor test with the material, lights and the projector. After setting up and raising the wire, we quickly noticed that the light frost cloth in the wind was flying up vertical even in the very light evening wind. Realising the problem, we frantically called structure and material to start brainstorming the solution. 

{% include image.html id="image2" images="/assets/img/festa/issue2.jpg" %}

I began looking at places to source all the equipment in Christchurch and made a spreadsheet to help with the organisation of it. We settled on TheLightSite to provide the lights and light mounting system (clamps, bars, tripods) and Shipleys to provide the projectors.


## In Christchurch

#### FRIDAY Setup

I was one of the few available to come down for Friday, and spent the day running around collecting materials/equipment and setting up. Considering it was just the 5 of us we managed to get a phenomenal about done including a lighting test Friday night. It was super exciting to see everything coming together.

{% include imageslider.html id="slider45" images=page.slider45 %}

#### Saturday Setup

  On Saturday it was another busy day of setting up but it was great to have the whole team down there.




{% include imageslider.html id="slider5" images=page.slider5 %}


#### ChOH Lab

Finally, the night arrived. It was time to relax a little bit and be good hosts to our guests. I ended up hanging around for pretty much the whole evening.

As the sun set and we could begin to see the colours of the lights coming through we all began to get really excited. Throughout the evening they got brighter and lit up the space wonderfully. As I insisted on having 7 lights, we were able to light up the middle area as well as Punky Brewster Bar.
{% include imageslider.html id="slider6" images=page.slider6 %}

We decided not to turn on the projectors until later in the evening as (as expected) they didn’t have any effect in the bright setting sun. Around 7:30 we returned to set them up but found it was still too light to be visible on the frost cloth. As a comprise we turned both projectors towards the centre pillar.
{% include imageslider.html id="slider7" images=page.slider7 %}

This looked amazing so we left it there until the sun had completely set and it was dark enough to focus them on the frost cloth.  After some trial and error, we finally got both projectors focused on the frost cloth. Holly’s videos and animations looked really good and the projectors added an element that would have been missed.
{% include imageslider.html id="slider8" images=page.slider8 %}
