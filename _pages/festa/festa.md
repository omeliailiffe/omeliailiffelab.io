---
category: pages
layout: olddefault
title: "Festa: Reflection"
permalink: /festa/
navbar: [Research, Concepts, Projections, Lighting, Concepts]
navbarbutton: [Blog, /festa-blog/]
slider0: [/assets/img/festa/concept2.jpg, /assets/img/festa/concept3.jpg, /assets/img/festa/concept4.jpg]
---
Below is my reflection on Festa 2018. Please also see my [blog](/festa-blog/) (updated weekly)

### Initial Thoughts
When I first heard about the opportunity to join the project, I had given very little thought to the future of food. I set about learning more about what our future might look like and found a few interesting things. I read about how, in the near future, farming might look very different,  about how most an unproportional amount of pollution comes from producing meat, and about how we can utilise different senses (such as touch, smell and sight) to affect how we taste and experience food.

### Initial Concepts
Once we narrowed our scope down to working with beer, it became easier to research and we could begin to produce concepts.
In the first rapid ideation session I was part of we created a maze system, through which a series of choices (based on senses) lead you the drink best suited for you. I created a tunnel mock up to highlight what it might look like.
{% include imageslider.html id="slider0" images=page.slider0 %}

Unfortunately I was away for the mid-semester break so missed out on the rapid prototyping sessions.

### Teams
Once FESTA teams established, what your team was addressing conceptual approach, design strategy, prototyping proof of concept developing at scale, or taking into the world

Our team was tasked with addressing **Projection, Lighting, and Audio**.
In our team we had Fiona, Olivia, Holly, Jacob, Kazè, Tara, and myself.

We worked together very well throughout the project, bouncing ideas off each other, working together to solve problems, and, most importantly, having fun.

#### Issues
We faced a number of issues throughout the 6 weeks of development together.
##### Projections
Our group was formed on the idea of projecting images onto the frost cloth and, as with any task, we had a range of issues.

Firstly, we discovered that creating our own imagery was a challenging task. We talked about if anyone would like to do it and both Holly and Jacob stepped up.

Another issue we faced was positioning the projectors. We initially envisioned the projectors positioned on top of each of the pillars facing the opposite frost cloth. However as we received updated diagrams from the structure group we realised that, due to the angle of the triangle, that placement would not be possible. After some deliberation and discussion with the structure team we discovered two preexisting fences on site where we could place stands and projectors with little risk of people walking into them.

Finally the last battle with the projectors was figuring out where we were going to source them. We quickly discovered that, due to Festa falling on the last weekend of the semester, we were unable to source them through Massey. This meant we had to hire from a company. Due to my large amount of knowledge and vision for what we would need to hire, this job fell to me. I created a spreadsheet to organise and help decide which company we would order from.
{% include image.html id="sheet2" images="/assets/img/festa/sheet2.png" %}


###### Dealing with these issues.
Looking back I am not happy and rather ashamed of how we handled these issues, in particular with the projector placement. Instead of trying every possibility we rather quickly dismissed projecting and put it in the to hard basket. If we hadn't of been pushed by our classmates to see it through a key part of the final installation would of been missing. This has been a big learning curve for me, to listen to my peers more, and to keep trying to see/make a way.


#### Testing and Learning
Testing was a key process for us and I think we were on of the most prepared groups on the day. We completely 4 full scale tests (3 inside and one outside) and numerous smaller scale tests, each learning more about the installation, getting opinions from our classmates and fine tuning our videos.
After setting it up multiply times I realised that we would need a document to guide however was setting on the day.
{% include image.html id="sheet1" images="/assets/img/festa/sheet1.png" %}

This is the document I created and it proved to be very helpful on the setup day.


### Festa Install
I was one of the few available to come down for Friday, and spent the day running around collecting materials/equipment and setting up. Considering it was just the 5 of us we managed to get a phenomenal about done including a lighting test Friday night. Although it was super exciting to see it all coming together it was also incredible exhausting and stressful.

#### Reflection on our installation

Judging by the people that I talked to and watched participate in our ChOH Lab, I think we created a very successful installation. It was clear to see our space was popular, as it remained packed and busy for the extent of the evening.
From a hosting perspective, I really enjoyed discussing the meaning and thought behind our installation and upon reflecting on our goals, I think we did an excellent job. In our blurb, it highlights that "CHOH-LAB is an environment in which people are immersed in a micro environment focused solely on the experience of beer, and encountering it on a microscopic scale, as though, by choosing to move into the space, you had entered the drink itself." With each of the groups coming together we were able to create a cohesive installation that fully lives up to this blurb.
Every aspect of the installation came together beautiful (the soundscape, the lighting, the projectors, the marbled frost cloth, the spatial type, the bar placement and beers, the forward-thinking kitchen, and the bar leaners) to create an experience that will not leave those that participated mind so easily.
